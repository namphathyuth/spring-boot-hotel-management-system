package com.kiloit.hms.config;

public enum RoomStatusEnum {
    AVAILABLE,
    OCCUPIED,
    RESERVED,
    MAINTENANCE
}
