package com.kiloit.hms.config.enumeration;

public enum BookingStatusEnum {
    PENDING,
    CONFIRMED,
    CANCELLED,
    CHECKED_IN,
    CHECKED_OUT
}
