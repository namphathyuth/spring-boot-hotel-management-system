package com.kiloit.hms.config.enumeration;

public enum PaymentMethodEnum {
    CASH,
    BANK_TRANSFER
}
