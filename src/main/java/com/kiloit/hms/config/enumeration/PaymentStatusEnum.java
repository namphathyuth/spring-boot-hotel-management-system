package com.kiloit.hms.config.enumeration;

public enum PaymentStatusEnum {
    PENDING,
    PAID,
    CANCELLED
}
