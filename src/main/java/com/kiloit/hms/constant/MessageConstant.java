package com.kiloit.hms.constant;

/**
 * @author Sombath
 * create at 23/1/24 3:48 PM
 */
public class MessageConstant {

    public static final String SUCCESSFULLY = "Successfully";
    public static final String ALL = "ALL";
    public static final String FAIL = "FAILED";
    public static  final String NOT_ALLOWED = "Not Allowed";

    public static final String FORBIDDEN = "Action Is Forbidden";

    public static class AUTH {
        public final static String INCORRECT_USERNAME_OR_PASSWORD = "Incorrect Username or password";
        public final static String ACCOUNT_DEACTIVATE = "Account have been deactivated";

        public final static String BAD_CREDENTIALS = "Bad credentials";

        public static final Object USER_REGISTERED = "register successfully";
    }
    public static class REVIEW{
        public static final Object ADD_SUCCESS = "you're feed back sent successfully!";
        public static final String ID_NOTFOUND = "review id not found!";
        public static final Object DELETED = "review deleted successfully!";
    }

    public static class ROLE {
        public final static String ADMIN = "ADMIN";
        public final static String USER = "USER";
        public static final String ROLE_CREATED_SUCCESSFULLY = "Role has been created";
        public static final String ROLE_ID_NOT_FOUND= "role id could not be found";
        public static final String ROLE_DELETED_SUCCESSFULLY = "Role has been deleted";

        public static final Object ROLES_ADDED = "role added successfully!";
        public static final Object ROLE_UPDATED = "role updated successfully!";
        public static final Object ROLE_DELETED = "role deleted successfully!";
        public static final String ROLE_NAME_EXISTED = "role name already existed";
        public static final String ROLE_CODE_EXISTED = "role code already existed";

    }

    public static class USERS{
        public static final String UERS_DELETED = "user deleted successfully";
        public static final String ID_NOT_FOUND = "Id could not found!";
        public static final Object UERS_UPDATED = "user updated successfully!";
        public static final Object USERS_ADDED = "user added successfully!";

        public static final String OLD_PASSWORD_WRONG = "the old password wrong!";

        public static final String SAME_PASSWORD = "the password still the same!";
        public static final Object USER_IS_UPDATE = "password reset successfully!";
        public static final String EMAIL_EXISTED = "the email already existed!";
        public static final String USERNAME_EXISTED ="the username already existed!";
        public static final String USER_NOT_FOUND = "User not found";
        public static final Object IMAGE_UPDATED = "image update successfully!";
        public static final String PROFILE_IMAGE_NOT_FOUND = "";
        public static final String FILE_IMAGE_EXCITED = "image already excite!";
        public static final String IMAGE_DELETED_FAIL = "image delete unsuccessful!";
        public static final Object IMAGE_DELETED = "image deleted successfully!";
        public static final Object IMAGE_ADDED = "image added successfully!";
        public static final String PROFILE_IMAGE_NOT_VALID = "image is valid!";
        public static final String IMAGE_TOO_LARGE = "image size is to large!";
    }

    public static class ROOM{
        public static final String ROOM_TYPE_NOT_FOUND = "Room type could not found";
        public static final String ROOM_NOT_AVAILABLE = "Requested room is not currently available";
        public static final String ROOM_NOT_FOUND = "Room could not found";
        public static final String IMAGE_NOT_FOUND = "Image could not found";
        public static final String INVALID_IMAGE = "Uploaded image is not valid";
        public static final String UPLOAD_IMAGE_EXCEED_LIMIT_SIZE = "Uploaded image exceed limited size";
    }

    public static class HOTEL{
        public static final String HOTEL_INFO_NOT_FOUND = "Hotel info could not found";
        public static final String DAILY_REPORT_NOT_FOUND = "Daily report could not foung";
    }

    public static class BOOKING{
        public static final String BOOKING_DELETED = "Booking Successfully Deleted";
        public static final String BOOKING_CANCELLED = "Booking Successfully Cancelled";
        public static final String BOOKING_NOT_FOUND = "Booking Not Found";
        public static final String BOOKING_UPDATED = "Booking Updated";
        public static final String BOOKING_DETAIL_CREATED = "Booking Detail Created";
        public static final String BOOKING_DETAIL_DELETED = "Booking Detail Deleted";
        public static final String BOOKING_DETAIL_NOT_FOUND = "Booking Detail Not Found";
        public static final String BOOKING_DETAIL_UPDATED = "Booking Detail Updated";
        public static final String CHECKED_IN = "Checked In";
        public static final String BOOKING_SUCCESS = "Booking Successful";
        public static final String CANCEL_NOT_ALLOWED = "Cancel Not Allowed";
        public static final String CHECK_OUT_NOT_ALLOWED = "Check out is not allowed";
        public static final String ID_NOTFOUND = "booking id not found!";
    }

    public static class PAYMENT{
        public static final String PAYMENT_NOT_FOUNT = "Payment Not Found";
        public static final String PAYMENT_DELETED = "Payment Deleted";
        public static final String PAYMENT_REQUIRED = "Payment Required";
        public static final String PAYMENT_UPDATED = "Payment Updated";
    }
}
