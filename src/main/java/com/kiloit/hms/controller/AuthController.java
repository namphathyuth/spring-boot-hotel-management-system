package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.auth.LoginRQ;
import com.kiloit.hms.model.request.auth.RegisterRQ;
import com.kiloit.hms.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author Sombath
 * create at 23/1/24 3:35 PM
 */

@RestController
@RequestMapping("api/auth")
@RequiredArgsConstructor
public class AuthController extends BaseController {

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<StructureRS> login(@Validated @RequestBody LoginRQ loginRQ) {
        return response(authService.login(loginRQ));
    }
    @PostMapping("/register")
    public ResponseEntity<StructureRS> register(@RequestBody @Validated RegisterRQ registerRQ){
        return response(authService.register(registerRQ));
    }

}



