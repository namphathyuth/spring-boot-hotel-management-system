package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.booking.BookingListingRQ;
import com.kiloit.hms.model.request.booking.BookingRQ;
import com.kiloit.hms.model.request.booking.UpdateBookingRQ;
import com.kiloit.hms.security.UserPrincipal;
import com.kiloit.hms.service.BookingService;
import com.kiloit.hms.service.CheckInService;
import com.kiloit.hms.service.CheckOutService;
import com.kiloit.hms.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.rmi.StubNotFoundException;

@RestController
@RequestMapping("api/booking")
@RequiredArgsConstructor
public class BookingController extends BaseController {
    private final BookingService bookingService;
    private final PaymentService paymentService;
    private final CheckInService checkInService;
    private final CheckOutService checkOutService;

    @PostMapping
    public ResponseEntity<StructureRS> booking(@RequestBody @Validated BookingRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        return response(bookingService.booking(request, jwtAuthenticationToken));
    }

    @GetMapping
    public ResponseEntity<StructureRS> getBooking(BookingListingRQ request){
        return response(bookingService.getBooking(request));
    }

    @GetMapping("{id}")
    public ResponseEntity<StructureRS> getBookingById(@PathVariable Long id){
        return response(bookingService.getBookingById(id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteBookingById(@PathVariable Long id){
        return response(bookingService.deleteBookingById(id));
    }
    @PutMapping("{id}")
    public ResponseEntity<StructureRS> updateBooking(@PathVariable Long id, @RequestBody @Validated BookingRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        return response(bookingService.editBooking(id, request, jwtAuthenticationToken));
    }

    @PostMapping("{id}/cancel")
    public ResponseEntity<StructureRS> cancelBooking(@PathVariable Long id, JwtAuthenticationToken jwtAuthenticationToken){
        return response(bookingService.cancelBooking(id, jwtAuthenticationToken));
    }

    @GetMapping("/my-booking")
    public ResponseEntity<StructureRS> getCurrentUserBooking(BookingListingRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        return  response(bookingService.getCurrentUserBooking(request, jwtAuthenticationToken));
    }

    @GetMapping("{id}/payment")
    public ResponseEntity<StructureRS> getBookingPayment(@PathVariable Long id){
        return response(paymentService.getPaymentByBookingId(id));
    }

    @PostMapping("{id}/check-in")
    public ResponseEntity<StructureRS> checkIn(@PathVariable Long id){
        return response(checkInService.checkIn(id));
    }

    @PostMapping("{id}/check-out")
    public ResponseEntity<StructureRS> checkOut(@PathVariable Long id){
        return response(checkOutService.checkOut(id));
    }

}
