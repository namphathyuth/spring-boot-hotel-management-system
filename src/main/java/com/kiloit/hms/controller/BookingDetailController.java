package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.db.reposiroty.BookingDetailRepository;
import com.kiloit.hms.model.request.booking.BookingDetailRQ;
import com.kiloit.hms.model.request.booking.UpdateBookingDetailRQ;
import com.kiloit.hms.service.BookingDetailService;
import lombok.RequiredArgsConstructor;
import org.hibernate.sql.Update;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/booking-detail")
public class BookingDetailController extends BaseController {
    private final BookingDetailService bookingDetailService;

    @GetMapping("by-booking/{bookingId}")
    public ResponseEntity<StructureRS> getBookingDetailByBookingId(@PathVariable Long bookingId){
        return response(bookingDetailService.getBookingDetailByBookingId(bookingId));
    }

    @PostMapping("{bookingId}")
    public ResponseEntity<StructureRS> createBookingDetail(@PathVariable Long bookingId, @RequestBody @Validated BookingDetailRQ request){
        return response(bookingDetailService.createBookingDetail(bookingId, request));
    }

    @PutMapping("{id}")
    public ResponseEntity<StructureRS> updateBookingDetail(@PathVariable Long id,@RequestBody @Validated UpdateBookingDetailRQ request){
        return response(bookingDetailService.updateBookingDetail(id, request));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteBookingDetailById(@PathVariable Long id){
        return response(bookingDetailService.deleteBookingDetailById(id));
    }

    @GetMapping("{id}")
    public ResponseEntity<StructureRS> getBookingDetailById(@PathVariable Long id){
        return response(bookingDetailService.getBookingDetailById(id));
    }
}
