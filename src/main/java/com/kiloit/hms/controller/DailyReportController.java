package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.room.ReportRQ;
import com.kiloit.hms.service.DailyReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/report")
public class DailyReportController extends BaseController {
    private final DailyReportService dailyReportService;

    @GetMapping("/total-booking")
    public ResponseEntity<StructureRS> getReport(ReportRQ request){
        return response(dailyReportService.getBooking(request));
    }

    @GetMapping("/total-income")
    public ResponseEntity<StructureRS> getIncome(ReportRQ request){
        return response(dailyReportService.getIncome(request));
    }

    @GetMapping("/room-booked")
    public ResponseEntity<StructureRS> getMaxRoom(ReportRQ request){
        return response(dailyReportService.getRoomBooked(request));
    }
}


