package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.hotel.HotelInfoRQ;
import com.kiloit.hms.service.HotelInfoService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1.0.0/hotelinfo")
public class HotelInfoController extends BaseController {
    private final HotelInfoService hotelInfoService;

    @GetMapping
    public ResponseEntity<StructureRS> getInfo(BaseListingRQ request){
        return response(hotelInfoService.getInfo(request));
    }

    @GetMapping("{id}")
    public ResponseEntity<StructureRS> getById(@PathVariable Long id){
        return response(hotelInfoService.getById(id));
    }

    @PostMapping
    public ResponseEntity<StructureRS> addInfo(@RequestBody @Valid HotelInfoRQ request){
        hotelInfoService.addInfo(request);
        return response();
    }

    @PutMapping("{id}")
    public ResponseEntity<StructureRS> editInfo(@RequestBody @Valid HotelInfoRQ request, @PathVariable Long id){
        hotelInfoService.editInfo(request, id);
        return response();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteInfo(@PathVariable Long id){
        hotelInfoService.deleteInfo(id);
        return response();
    }
}
