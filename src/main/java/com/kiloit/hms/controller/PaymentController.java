package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.payment.PaymentListingRQ;
import com.kiloit.hms.model.request.payment.UpdatePaymentRQ;
import com.kiloit.hms.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.rmi.StubNotFoundException;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/payment")
public class PaymentController extends BaseController {
    private final PaymentService paymentService;

    @GetMapping("by-booking/{bookingId}")
    public ResponseEntity<StructureRS> getPaymentByBookingId(@PathVariable Long bookingId){
        return response(paymentService.getPaymentByBookingId(bookingId));
    }

    @GetMapping()
    public ResponseEntity<StructureRS> getAllPayment(PaymentListingRQ request){
        return response(paymentService.getPayment(request));
    }

    @GetMapping("{id}")
    public ResponseEntity<StructureRS> getPaymentById(@PathVariable Long id){
        return response(paymentService.getPaymentById(id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deletePaymentById(@PathVariable Long id){
        return response(paymentService.deletePaymentById(id));
    }

    @PutMapping("{id}")
    public ResponseEntity<StructureRS> updatePayment(@PathVariable Long id, @RequestBody @Validated UpdatePaymentRQ request){
        return response(paymentService.updatePayment(id, request));
    }

}




