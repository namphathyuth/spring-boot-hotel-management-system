package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.hotel.ReviewRQ;
import com.kiloit.hms.service.ReviewService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/review")
@RestController
@RequiredArgsConstructor
public class ReviewController extends BaseController {
    private final ReviewService reviewService;
    @GetMapping
    public ResponseEntity<StructureRS> getReview(BaseListingRQ baseListingRQ){
        return response(reviewService.getReview(baseListingRQ));
    }
    @GetMapping("{id}")
    public ResponseEntity<StructureRS> getReviewId(@PathVariable Long id){
        return response(reviewService.getReviewId(id));
    }
    @PostMapping
    public ResponseEntity<StructureRS> addReview(@RequestBody @Valid ReviewRQ reviewRQ){
        return response(reviewService.addReview(reviewRQ));
    }
    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteReview(@PathVariable Long id){
        return response(reviewService.deleteReview(id));
    }
}
