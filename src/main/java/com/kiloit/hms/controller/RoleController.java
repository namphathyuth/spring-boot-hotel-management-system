package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.role.PermissionRQ;
import com.kiloit.hms.model.request.role.RoleRQ;
import com.kiloit.hms.model.request.role.UpdateRolePermissionRQ;
import com.kiloit.hms.model.request.role.UpdateRoleRQDto;
import com.kiloit.hms.service.RoleService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Sombath
 * create at 23/1/24 3:35 PM
 */
@RequestMapping("api/roles")
@RestController
@RequiredArgsConstructor
public class RoleController extends BaseController {

    private final RoleService roleService;

    @GetMapping
    public ResponseEntity<StructureRS> getRoles(BaseListingRQ request){
        return response(roleService.getRoles(request));
    }
    @GetMapping("{id}")
    public ResponseEntity<StructureRS> shoeById(@PathVariable Long id){
        return response(roleService.getRoleById(id));
    }
    @PostMapping
    public ResponseEntity<StructureRS> addRole(@RequestBody @Valid RoleRQ request){
        return response(roleService.addRole(request));
    }
    @PutMapping("{id}")
    public ResponseEntity<StructureRS> updateRole(@Valid @RequestBody UpdateRoleRQDto updateRoleRQDto, @PathVariable Long id ){
        return response(roleService.updateRole(updateRoleRQDto,id));
    }
    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteRole(@PathVariable Long id){
        return response(roleService.deleteRole(id));
    }
    @PutMapping
    public ResponseEntity<StructureRS> updatePermission(@RequestBody @Valid UpdateRolePermissionRQ updateRolePermissionRQ){
        return response(roleService.updatePermission(updateRolePermissionRQ));
    }

}



