package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.room.RoomRQ;
import com.kiloit.hms.service.RoomService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1.0.0/room")
public class RoomController extends BaseController {
    private final RoomService roomService;

    @GetMapping
    public ResponseEntity<StructureRS> getRoom(@RequestParam(required = false, defaultValue = "1024-02-16T08:19:08Z") Instant startDate,@RequestParam(required = false, defaultValue = "3024-02-16T08:19:08Z") Instant endDate, BaseListingRQ request){
        return response(roomService.getRoom(startDate, endDate, request));
    }

    @GetMapping("{id}")
    public ResponseEntity<StructureRS> getById(@PathVariable Long id){
        return response(roomService.getById(id));
    }

    @PostMapping
    public ResponseEntity<StructureRS> addRoom(@RequestBody @Valid RoomRQ request){
        roomService.addRoom(request);
        return response();
    }

    @PutMapping("{id}")
    public ResponseEntity<StructureRS> editRoom(@PathVariable Long id, @Valid @RequestBody RoomRQ request){
        roomService.editRoom(id,request);
        return response();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteRoomType(@PathVariable Long id){
        return response(roomService.deleteRoom(id));
    }
}
