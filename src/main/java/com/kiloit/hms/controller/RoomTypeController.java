package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.room.RoomTypeListingRQ;
import com.kiloit.hms.model.request.room.RoomTypeRQ;
import com.kiloit.hms.service.RoomTypeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1.0.0/room-type")
public class RoomTypeController extends BaseController {
    private final RoomTypeService roomTypeService;

    @GetMapping
    public ResponseEntity<StructureRS> getRoomType(RoomTypeListingRQ request) {
        return response(roomTypeService.getRoomType(request));
    }

    @GetMapping("{id}")
    public ResponseEntity<StructureRS> getById(@PathVariable Long id) {
        return response(roomTypeService.getById(id));
    }

    @PostMapping
    public ResponseEntity<StructureRS> addRoomType(@RequestBody @Valid RoomTypeRQ request) {
        return response(roomTypeService.addRoomType(request));
    }

    @PutMapping("{id}")
    public ResponseEntity<StructureRS> editRoomType(@PathVariable Long id, @Valid @RequestBody RoomTypeRQ request) throws IOException {
        roomTypeService.editRoomType(id, request);
        return response();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteRoomType(@PathVariable Long id) {
        roomTypeService.deleteRoomType(id);
        return response();
    }

    @PostMapping("{roomTypeId}/image")
    public ResponseEntity<StructureRS> addImage(@RequestParam("images") MultipartFile[] images, @PathVariable Long roomTypeId){
        return response(roomTypeService.addRoomTypeImage(roomTypeId, images));
    }

    @DeleteMapping("image/{id}")
    public ResponseEntity<StructureRS> deleteImageById(@PathVariable Long id){
        return response(roomTypeService.deleteRoomTypeImage(id));
    }
//    @PostMapping
//    public ResponseEntity<StructureRS> createRoomType(@ModelAttribute @Validated RoomTypeRQ request, @RequestParam("images") MultipartFile[] images){
//        return response(roomTypeService.createRoomType(request, images));
//    }

//    @PutMapping("{roomTypeId}")
//    public ResponseEntity<StructureRS> updateRoomType(@PathVariable Long roomTypeId, @ModelAttribute @Validated RoomTypeRQ request, @RequestParam("images") MultipartFile[] images){
//        return response(roomTypeService.updateRoomType(roomTypeId, request, images));
//    }
}

