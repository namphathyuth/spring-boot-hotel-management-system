package com.kiloit.hms.controller;

import com.kiloit.hms.base.BaseController;
import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.model.request.auth.PasswordResetRQ;
import com.kiloit.hms.model.request.user.UpdateProfileRQDto;
import com.kiloit.hms.model.request.user.UpdateUserRQ;
import com.kiloit.hms.model.request.user.UpdateUserRQDto;
import com.kiloit.hms.model.request.user.UserRQ;
import com.kiloit.hms.security.UserPrincipal;
import com.kiloit.hms.service.Userservice;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RequestMapping("api/users")
@RestController
@RequiredArgsConstructor
public class UserController extends BaseController {

    private final Userservice userservice;

    @GetMapping
    public ResponseEntity<StructureRS> getUser(BaseListingRQ baseListingRQ,@RequestParam(required = false, defaultValue = "0") Long roleId){
        return response(userservice.getUser(baseListingRQ,roleId));
    }
    @GetMapping("{id}")
    public ResponseEntity<StructureRS> showById(@PathVariable Long id){
        return response(userservice.showById(id));
    }
    @PostMapping
    public ResponseEntity<StructureRS> addUser( @Validated @RequestBody UserRQ userRQ){
        return  response(userservice.addUser(userRQ));
    }
    @PutMapping("{id}")
    public ResponseEntity<StructureRS> updateUser(@RequestBody @Validated UpdateUserRQDto updateUserRQDto, @PathVariable Long id){
        return response(userservice.updateUser(updateUserRQDto,id));
    }
    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteUser(@PathVariable Long id){
        return response(userservice.deleteUser(id));
    }

    @PutMapping("change-password/{id}")
    public ResponseEntity<StructureRS> changePasswordUser(@PathVariable Long id,@Validated @RequestBody PasswordResetRQ request){
        return response(userservice.changePasswordUser(id, request));
    }
    @PutMapping
    public ResponseEntity<StructureRS> resetPassword(@Validated @RequestBody PasswordResetRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        return response(userservice.resetPassword(request, UserPrincipal.build(jwtAuthenticationToken)));
    }

    @PostMapping("image/{id}")
    public ResponseEntity<StructureRS> addImage(@RequestParam("image") MultipartFile adImages, @PathVariable Long id) throws IOException {
        userservice.addImage(adImages,id);
        return response();
    }
    @PostMapping(value = "image", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<StructureRS> addImageByToken(JwtAuthenticationToken jwtAuthenticationToken,@RequestPart("image") MultipartFile file) throws IOException{
        return response(userservice.addImageByToken(UserPrincipal.build(jwtAuthenticationToken),file));
    }
    @DeleteMapping("image")
    public ResponseEntity<StructureRS> deleteImageByToken(JwtAuthenticationToken jwtAuthenticationToken) throws IOException{
        return response(userservice.deleteImageByToken(UserPrincipal.build(jwtAuthenticationToken)));
    }

    @GetMapping("getProfile")
    public ResponseEntity<StructureRS> getProfile( JwtAuthenticationToken jwtAuthenticationToken){
        return response(userservice.getProfile(UserPrincipal.build(jwtAuthenticationToken)));
    }
    @PutMapping("updateProfile")
    public ResponseEntity<StructureRS> updateProfile(@RequestBody @Valid UpdateProfileRQDto updateProfileRQDto, JwtAuthenticationToken jwtAuthenticationToken){
        return response(userservice.updateProfile(updateProfileRQDto, UserPrincipal.build(jwtAuthenticationToken)));
    }

}
