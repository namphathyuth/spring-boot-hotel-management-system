package com.kiloit.hms.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kiloit.hms.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

@Getter
@Setter
@Entity
@Table(name = "booking_detail")
@SQLDelete(sql = "UPDATE booking_detail SET deleted_at = now() WHERE id = ?")
@SQLRestriction("deleted_at is null")
public class BookingDetailEntity extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booking_id")
    private BookingEntity booking;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_type_id")
    private RoomTypeEntity roomType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private RoomEntity room;

    @Column(name = "adult")
    private int adult;

    @Column(name = "child")
    private int child;
}