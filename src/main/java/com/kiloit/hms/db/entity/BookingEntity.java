package com.kiloit.hms.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kiloit.hms.base.BaseEntity;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "booking")
@SQLDelete(sql = "UPDATE booking SET deleted_at = now() WHERE id = ?")
@SQLRestriction("deleted_at is null")
public class BookingEntity extends BaseEntity {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "check_in")
    private Date checkInAt;
    @Column(name = "check_out")
    private Date checkOutAt;


    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private BookingStatusEnum status;
    @Column(name = "special_request")
    private String specialRequest;

    @OneToMany(mappedBy = "booking", cascade = CascadeType.ALL)
    private List<BookingDetailEntity> bookingDetail = new ArrayList<>();

    @OneToOne(mappedBy = "booking", cascade = CascadeType.ALL)
    private PaymentEntity payment;
}