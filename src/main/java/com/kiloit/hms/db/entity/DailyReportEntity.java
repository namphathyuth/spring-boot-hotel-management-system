package com.kiloit.hms.db.entity;

import com.kiloit.hms.base.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "daily_report")
public class DailyReportEntity extends BaseEntity {
    private Integer totalBooking;
    private Integer totalCancel;
    private double totalIncome;
    private double totalExpense;
}