package com.kiloit.hms.db.entity;

import com.kiloit.hms.base.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "hotel_info")
public class HotelInfoEntity extends BaseEntity {

    @Column(name = "title")
    @Size(max = 100)
    private String title;

    @Column(name = "sub_title")
    @Size(max = 100)
    private String subTitle;

    @Column(name = "description")
    private String description;

    @Column(name = "thumbnail_url")
    private String thumbnailUrl;
}