package com.kiloit.hms.db.entity;

import com.kiloit.hms.base.BaseEntity;
import com.kiloit.hms.config.enumeration.PaymentMethodEnum;
import com.kiloit.hms.config.enumeration.PaymentStatusEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "payment")
@SQLDelete(sql = "UPDATE payment SET deleted_at = now() WHERE id = ?")
@SQLRestriction("deleted_at is null")
public class PaymentEntity extends BaseEntity {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booking_id", referencedColumnName = "id")
    private BookingEntity booking;
    private double totalPayment;
    @Enumerated(EnumType.STRING)
    private PaymentStatusEnum paymentStatus;
    @Enumerated(EnumType.STRING)
    private PaymentMethodEnum paymentMethod;
    private Date paymentDate;
    private String remark;

}