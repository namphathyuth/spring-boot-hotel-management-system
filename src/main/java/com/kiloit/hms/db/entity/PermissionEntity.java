package com.kiloit.hms.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "permission", indexes = {
        @Index(name = "idx_permission_name", columnList = "name")
})
@Setter
@Getter
public class PermissionEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "module", nullable = false)
    private String module;

    @JsonIgnore
    @ManyToMany(mappedBy = "permissionEntities")
    private Set<RoleEntity> roles;
}
