package com.kiloit.hms.db.entity;

import com.kiloit.hms.base.BaseEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "review")
@Setter
@Getter
public class ReviewEntity extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;
    @Column(name = "title")
    @Size(max = 255)
    private String title;
    @Column(name = "description")
    private String description;
}
