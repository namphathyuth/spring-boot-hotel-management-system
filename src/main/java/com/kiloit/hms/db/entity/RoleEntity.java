package com.kiloit.hms.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiloit.hms.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "role", indexes = {
        @Index(name = "idx_role_name", columnList = "name", unique = true)
})
@Getter
@Setter
@SQLDelete(sql = "UPDATE role SET deleted_at = now() WHERE id = ?")
public class RoleEntity extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @JsonIgnore
    @OneToMany(mappedBy = "roleEntity", fetch = FetchType.LAZY)
    private Set<UserEntity> userEntities;

    @ManyToMany
    @JoinTable(
            name = "roles_has_permissions",
            joinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "permission_id", referencedColumnName = "id"))
    @JsonProperty("permissions")
    private List<PermissionEntity> permissionEntities = new ArrayList<>();
}
