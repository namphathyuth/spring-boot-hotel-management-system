package com.kiloit.hms.db.entity;

import com.kiloit.hms.base.BaseEntity;
import com.kiloit.hms.config.RoomStatusEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;

@Entity
@Table(name = "room")
@Getter
@Setter
@SQLDelete(sql = "update room set deleted_at = now() where id = ?")
@SQLRestriction("deleted_at is null")
public class RoomEntity extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_type_id", nullable = false)
    private RoomTypeEntity roomType;

    @Column(name = "floor")
    private String floor;

    @Column(name = "room")
    private String room;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private RoomStatusEnum status;
}
