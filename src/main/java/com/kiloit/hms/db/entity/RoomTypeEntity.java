package com.kiloit.hms.db.entity;

import com.kiloit.hms.base.BaseEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "room_type")
@Setter
@Getter
@SQLDelete(sql = "update room_type set deleted_at = now() where id = ?")
@SQLRestriction("deleted_at is null")
public class RoomTypeEntity extends BaseEntity {
    @Column(name = "title")
    @Size(max = 100)
    private String title;

    @Column(name = "sub_title")
    @Size(max = 100)
    private String subTitle;

    @Column(name = "description")
    private String description;

    @Column(name = "bed")
    private int bed;

    @Column(name = "adult")
    private int adult;

    @Column(name = "children")
    private int children;

    @Column(name = "price")
    private double price;

    @Column(name = "amenity")
    private String amenity;

    @OneToMany(mappedBy = "roomType", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RoomTypeImageEntity> image = new ArrayList<>();

    @OneToMany(mappedBy = "roomType", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RoomEntity> rooms = new ArrayList<>();
}
