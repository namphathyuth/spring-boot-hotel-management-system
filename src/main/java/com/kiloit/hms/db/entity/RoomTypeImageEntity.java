package com.kiloit.hms.db.entity;

import com.kiloit.hms.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "room_type_image")
@Setter
@Getter
public class RoomTypeImageEntity extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_type_id", nullable = false)
    private RoomTypeEntity roomType;

    @Column(name = "url")
    private String url;
}
