package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.config.RoomStatusEnum;
import com.kiloit.hms.db.entity.BookingDetailEntity;
import com.kiloit.hms.db.entity.RoomEntity;
import com.kiloit.hms.model.projection.BookingDetailEntityInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface BookingDetailRepository extends JpaRepository<BookingDetailEntity, Long> {
//    @Query(value = "select count (bd) from BookingDetailEntity bd where bd.roomType.id = :roomTypeId " +
//            "and (bd.booking.checkInAt >= :checkIn and bd.booking.checkOutAt <= :checkOut) " +
//            "or (bd.booking.checkInAt <= :checkIn and bd.booking.checkOutAt >= :checkOut) " +
//            "or (bd.booking.checkInAt < :checkOut and bd.booking.checkOutAt >= :checkOut)")
//    @Query(value = "select count (bd) from BookingDetailEntity bd where bd.roomType.id = :roomTypeId and " +
//            "(bd.booking.checkInAt <= :checkOut and bd.booking.checkOutAt >= :checkIn) or " +
//            "(bd.booking.checkInAt <= :checkIn and bd.booking.checkOutAt >= :checkIn)")
    @Query(value = "select count (bd) from BookingDetailEntity bd join BookingEntity b on bd.booking.id = b.id where " +
            "bd.roomType.id = :roomTypeId and (extract(date from bd.booking.checkInAt) <= extract(date from :checkOut) and extract(date from bd.booking.checkOutAt) >= extract(date from :checkIn) ) and " +
            "(extract(date from bd.booking.checkInAt) <= extract(date from :checkIn) and extract(date from bd.booking.checkOutAt) >= extract(date from :checkIn) ) and " +
            "(b.status = 'CONFIRMED')")
    int countAllBookingDetailBetween(Long roomTypeId, Date checkIn, Date checkOut);

    @Query(value = "select count (room) from RoomEntity room where room.roomType.id = :roomTypeId and room.status not in :roomStatus")
    int countAllRoomByRoomTypeWhereStatusNotIn(Long roomTypeId, List<RoomStatusEnum> roomStatus);

    List<BookingDetailEntityInfo> getBookingDetailEntitiesByBookingId(Long bookingId);
    Optional<BookingDetailEntityInfo> getBookingDetailById(Long id);

    void deleteAllByBookingId(Long bookingId);
}