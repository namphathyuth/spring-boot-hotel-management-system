package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.config.RoomStatusEnum;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import com.kiloit.hms.db.entity.BookingEntity;
import com.kiloit.hms.model.projection.BookingEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<BookingEntity, Long> {
    @Query(value = "select b from BookingEntity b left join fetch b.user where " +
            "(:checkIn is null or extract(date from b.checkInAt) = extract(date from :checkIn)) and " +
            "(:checkOut is null or extract(date from b.checkOutAt) = extract(date from :checkOut)) and " +
            "(:username is null or b.user.username like :username) and (:status is null or b.status = :status)")
    Page<BookingEntityInfo> getAllBookingWithFilter(Date checkIn, Date checkOut, String username, BookingStatusEnum status, Pageable pageable);

    @Query(value = "select b from BookingEntity b left join fetch b.user where " +
            "(:checkIn is null or extract(date from b.checkInAt) = extract(date from :checkIn)) and " +
            "(:checkOut is null or extract(date from b.checkOutAt) = extract(date from :checkOut)) and " +
            "(:username is null or b.user.username like :username) and (:status is null or b.status = :status)")
    Page<BookingEntity> getAllBookingEntityWithFilter(Date checkIn, Date checkOut, String username, BookingStatusEnum status, Pageable pageable);

    @Query(value = "select b from BookingEntity b left join fetch b.user where b.id = :id")
    Optional<BookingEntityInfo> getBookingById(Long id);

}