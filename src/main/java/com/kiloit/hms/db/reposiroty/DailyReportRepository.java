package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.db.entity.DailyReportEntity;
import com.kiloit.hms.model.projection.ReportEnityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Date;

public interface DailyReportRepository extends JpaRepository<DailyReportEntity, Long> {
    @Query("select count(booking.id) as totalBooking from BookingEntity booking " +
            "where booking.deletedAt is null " +
            "and extract(date from booking.createdAt) between extract(date from :startDate) and extract(date from :endDate) ")
    Page<ReportEnityInfo> findDailyReportEntitiesByTotalBooking(Date startDate, Date endDate, Pageable pageable);
    @Query("select sum(payment.totalPayment) as totalIncome from PaymentEntity payment " +
            "where payment.deletedAt is null " +
            "and extract(date from payment.createdAt) between extract(date from :startDate) and extract(date from :endDate) ")
    Page<ReportEnityInfo> findDailyReportEntitiesByTotalIncome(Date startDate, Date endDate, Pageable pageable);
    @Query("select count(rb.id) as roomBooked, rb.roomType.title as roomType from BookingDetailEntity rb " +
            "where rb.deletedAt is null " +
            "and extract(date from rb.createdAt) between extract(date from :startDate) and extract(date from :endDate) " +
            "group by rb.roomType.title ")
    Page<ReportEnityInfo> findDailyReportEntitiesByRoomBooked(Date startDate, Date endDate, Pageable pageable);
}