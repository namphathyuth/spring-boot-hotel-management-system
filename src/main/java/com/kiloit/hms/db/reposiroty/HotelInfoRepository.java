package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.db.entity.HotelInfoEntity;
import com.kiloit.hms.model.projection.HotelInfoEntityInfo;
import com.kiloit.hms.model.projection.RoomEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface HotelInfoRepository extends JpaRepository<HotelInfoEntity, Long> {
    @Query("select i from HotelInfoEntity i where (:query = 'all' or i.title like concat(:query, '%'))")
    Page<HotelInfoEntityInfo> findHotelInfoEntitiesByTitle(String query, Pageable pageable);
    @Query("select i from HotelInfoEntity i where :id = i.id")
    HotelInfoEntityInfo findByIdInfo(Long id);
}