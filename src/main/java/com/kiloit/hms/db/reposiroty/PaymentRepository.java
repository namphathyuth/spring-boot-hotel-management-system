package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.config.enumeration.PaymentStatusEnum;
import com.kiloit.hms.db.entity.PaymentEntity;
import com.kiloit.hms.model.projection.PaymentEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface PaymentRepository extends JpaRepository<PaymentEntity, Long> {
    Optional<PaymentEntityInfo> getByBookingId(Long bookingId);
    @Query(value = """
    select p from PaymentEntity p where (:status is null or p.paymentStatus = :status)
    and (:username is null or p.booking.user.name = concat(:username, '%'))
    and (:paymentDate is null or extract(date from p.paymentDate) = extract(date from :paymentDate))
    """)
    Page<PaymentEntityInfo> getPaymentsWithFilter(PaymentStatusEnum status, String username, Date paymentDate, Pageable pageable);

    Optional<PaymentEntityInfo> getPaymentEntityById(Long id);
}