package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.db.entity.PermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface PermissionEntityRepository extends JpaRepository<PermissionEntity, Long> {
    @Query("select p from PermissionEntity p where p.id in :ids")
    List<PermissionEntity> findByIdIn(@Param("ids") Collection<Long> ids);


}