package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.db.entity.ReviewEntity;
import com.kiloit.hms.model.projection.ReviewEntityInfo;
import com.kiloit.hms.model.projection.UserEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReviewEntityRepository extends JpaRepository<ReviewEntity, Long> {
    @Query("select r from ReviewEntity r where (:title = 'all' or r.title like concat (:title ,'%'))")
    Page<ReviewEntityInfo> findByTitleLike(@Param("title") String title, Pageable pageable);
    @Query("select r from ReviewEntity r where :id = r.id")
    ReviewEntityInfo findByIdReview(Long id);
}