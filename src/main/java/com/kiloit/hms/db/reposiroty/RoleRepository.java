package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.db.entity.RoleEntity;
import com.kiloit.hms.model.projection.RoleEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNullApi;

import java.util.Optional;

//@RepositoryRestResource(collectionResourceRel = "roles", path = "roles")
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    @Query("select r from RoleEntity r left join fetch r.permissionEntities where (:query = 'all' or r.name like concat(:query, '%')) and (:query = 'all' or r.code like concat(:query, '%')) and r.deletedAt is null")
//    Page<RoleEntityInfo> findByNameStartsWithAndCode(String query, Pageable pageable);
    Page<RoleEntityInfo> findByNameStartsWithAndCode(String query, Pageable pageable);


    @Query("select r from RoleEntity r left join fetch r.permissionEntities where r.id = ?1")
    RoleEntity findByIdFetchPermission(Long id);

    @Query("select r from RoleEntity r where :id = r.id and r.deletedAt is null")
    Optional<RoleEntityInfo> findByIdRole(Long id);

    boolean existsByName(String name);
    boolean existsByCode(String code);

    @Query("select r from RoleEntity r where r.id = :id and r.deletedAt is null")
    Optional<RoleEntity> findRoleEntityById(Long id);
}