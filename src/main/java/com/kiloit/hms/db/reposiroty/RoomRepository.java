


package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.config.RoomStatusEnum;
import com.kiloit.hms.db.entity.RoomEntity;
import com.kiloit.hms.db.entity.RoomTypeEntity;
import com.kiloit.hms.model.projection.RoomEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, Long> {
    @Query("select room from RoomEntity room where (:query = 'all' or room.room like concat(:query, '%')) " +
            "or (:query = 'all' or LOWER(room.status) like concat(:query, '%'))" +
            "and room.createdAt between :startDate and :endDate")
    Page<RoomEntityInfo> findRoomEntitiesByRoom(Instant startDate, Instant endDate, String query, Pageable pageable);
    @Query("select room from RoomEntity room where :id = room.id")
    Optional<RoomEntityInfo> findByIdRoom(Long id);

    Optional<RoomEntity> getFirstByRoomTypeAndStatusIs(RoomTypeEntity roomTypeEntity, RoomStatusEnum status);
}