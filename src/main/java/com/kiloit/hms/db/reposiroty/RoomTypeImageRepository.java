package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.db.entity.RoomTypeImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface RoomTypeImageRepository extends JpaRepository<RoomTypeImageEntity, Long> {
    Optional<RoomTypeImageEntity> findByUrl(String url);
}