package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.db.entity.RoomTypeEntity;
import com.kiloit.hms.model.projection.RoomTypeEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface RoomTypeRepository extends JpaRepository<RoomTypeEntity, Long> {
    @Query("select rt from RoomTypeEntity rt where (:query = 'all' or rt.title like concat(:query, '%')) and " +
            "(:bed = 0 or rt.bed = :bed) and (:adult = 0 or rt.adult = :adult) and " +
            "(:amenity is null or rt.amenity like concat('%', :amenity, '%'))")
    Page<RoomTypeEntityInfo> findRoomTypeEntitiesByTitle(String query, int adult, int bed, String amenity, Pageable pageable);

    @Query("select rt from RoomTypeEntity rt where :id = rt.id")
    RoomTypeEntityInfo findByIdRoomType(Long id);
}