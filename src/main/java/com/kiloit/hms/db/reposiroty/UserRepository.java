package com.kiloit.hms.db.reposiroty;

import com.kiloit.hms.db.entity.UserEntity;
import com.kiloit.hms.model.projection.UserEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @Query("select u from UserEntity u join fetch u.roleEntity left join fetch u.createdBy  where (:query = 'all' or u.name like concat(:query, '%')) and (:roleId = 0 or u.roleEntity.id = :roleId) ")
    Page<UserEntityInfo> findByNameStartsWith( String query, Pageable pageable,Long roleId);

    @Query("select u from UserEntity u join fetch u.roleEntity role left join fetch role.permissionEntities where u.username = :username")
    UserEntity findByUsernameFetchRolePermission(String username);
    @Query("select u from UserEntity u where :id = u.id")
    UserEntityInfo findByIdUser(Long id);
    boolean existsByEmail(String email);

    boolean existsByUsername(String username);


    @Query("select u from UserEntity u where u.id = :idProfile")
    Optional<UserEntityInfo> findByIdProfile(@Param("idProfile") Long idProfile);


    @Query(value = """
    select count(*) from users where email = :email
    """, nativeQuery = true)
    long existsByEmailIgnoreSoftDelete(String email);

    @Query(value = """
    select count(*) from users where username = :username
    """, nativeQuery = true)
    long existsByUsernameIgnoreSoftDelete(String username);

    @Query(value = """
        select count(*) from users where username = :username and id != :userId
    """, nativeQuery = true)
    Long existsByUsernameIgnoreUserIdAndSoftDelete(String username, Long userId);

    @Query(value = """
        select count(*) from users where email = :email and id != :userId
    """, nativeQuery = true)
    Long existsByEmailIgnoreUserIdAndSoftDelete(String email, Long userId);
}