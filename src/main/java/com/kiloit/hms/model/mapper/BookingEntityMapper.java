package com.kiloit.hms.model.mapper;

import com.kiloit.hms.db.entity.BookingEntity;
import com.kiloit.hms.model.response.CurrentUserBookingDTO;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface BookingEntityMapper {
    BookingEntity toEntity(CurrentUserBookingDTO currentUserBookingDTO);

    @AfterMapping
    default void linkBookingDetail(@MappingTarget BookingEntity bookingEntity) {
        bookingEntity.getBookingDetail().forEach(bookingDetail -> bookingDetail.setBooking(bookingEntity));
    }

    CurrentUserBookingDTO toDto(BookingEntity bookingEntity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    BookingEntity partialUpdate(CurrentUserBookingDTO currentUserBookingDTO, @MappingTarget BookingEntity bookingEntity);
}