package com.kiloit.hms.model.mapper;

import com.kiloit.hms.db.entity.BookingEntity;
import com.kiloit.hms.db.entity.PaymentEntity;
import com.kiloit.hms.model.response.BookingResponse;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface BookingResponseMapper {
    BookingEntity toEntity(BookingResponse bookingResponse);

    @AfterMapping
    default void linkBookingDetail(@MappingTarget BookingEntity bookingEntity) {
        bookingEntity.getBookingDetail().forEach(bookingDetail -> bookingDetail.setBooking(bookingEntity));
    }

    @AfterMapping
    default void linkLinkPayment(@MappingTarget PaymentEntity paymentEntity) {
    }

    BookingResponse toDto(BookingEntity bookingEntity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    BookingEntity partialUpdate(BookingResponse bookingResponse, @MappingTarget BookingEntity bookingEntity);
}