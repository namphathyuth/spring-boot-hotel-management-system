package com.kiloit.hms.model.mapper;

import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.RoleEntity;
import com.kiloit.hms.db.reposiroty.RoleRepository;
import com.kiloit.hms.exception.httpstatus.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoleMapper {
    private final RoleRepository roleRepository;

    public RoleEntity toEntity(Long id) {
        return roleRepository.findById(id).orElseThrow(() -> new NotFoundException(MessageConstant.ROLE.ROLE_ID_NOT_FOUND));
    }
}
