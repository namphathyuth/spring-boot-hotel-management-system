package com.kiloit.hms.model.mapper;

import com.kiloit.hms.db.entity.UserEntity;
import com.kiloit.hms.model.request.user.UpdateProfileRQDto;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING, uses = {RoleMapper.class})
public interface UpdateProfileMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UserEntity partialUpdate(UpdateProfileRQDto updateProfileRQDto, @MappingTarget UserEntity userEntity);
}
