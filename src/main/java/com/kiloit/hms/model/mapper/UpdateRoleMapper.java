package com.kiloit.hms.model.mapper;


import com.kiloit.hms.db.entity.RoleEntity;
import com.kiloit.hms.model.request.role.UpdateRoleRQDto;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface UpdateRoleMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RoleEntity partialUpdate(UpdateRoleRQDto updateRoleRQDto, @MappingTarget RoleEntity roleEntity);
}
