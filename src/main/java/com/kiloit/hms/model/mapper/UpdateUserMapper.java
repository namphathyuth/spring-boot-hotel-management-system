package com.kiloit.hms.model.mapper;

import com.kiloit.hms.db.entity.RoleEntity;
import com.kiloit.hms.db.entity.UserEntity;
import com.kiloit.hms.model.request.user.UpdateUserRQDto;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING, uses = {RoleMapper.class})
public interface UpdateUserMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "roleEntity", source = "roleId")
    UserEntity partialUpdate(UpdateUserRQDto updateUserRQDto, @MappingTarget UserEntity userEntity);

    default RoleEntity map(Long id, RoleMapper roleMapper){
        return roleMapper.toEntity(id);
    }
}
