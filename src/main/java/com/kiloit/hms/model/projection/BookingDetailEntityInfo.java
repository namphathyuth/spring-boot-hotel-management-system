package com.kiloit.hms.model.projection;

import com.kiloit.hms.config.RoomStatusEnum;

import java.time.Instant;

/**
 * Projection for {@link com.kiloit.hms.db.entity.BookingDetailEntity}
 */
public interface BookingDetailEntityInfo {
    Long getId();

    Instant getCreatedAt();

    Instant getUpdatedAt();

    int getAdult();

    int getChild();

    UserEntityInfo getCreatedBy();

    RoomTypeEntityInfo getRoomType();

    RoomEntityInfo getRoom();

    /**
     * Projection for {@link com.kiloit.hms.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        Long getId();

        String getName();
    }

    /**
     * Projection for {@link com.kiloit.hms.db.entity.RoomTypeEntity}
     */
    interface RoomTypeEntityInfo {
        Long getId();

        String getTitle();

        double getPrice();
    }

    /**
     * Projection for {@link com.kiloit.hms.db.entity.RoomEntity}
     */
    interface RoomEntityInfo {
        Long getId();

        String getFloor();

        String getRoom();

        RoomStatusEnum getStatus();
    }
}