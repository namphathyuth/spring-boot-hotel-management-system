package com.kiloit.hms.model.projection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;

import java.time.Instant;
import java.util.Date;

/**
 * Projection for {@link com.kiloit.hms.db.entity.BookingEntity}
 */
public interface BookingEntityInfo {
    Long getId();
    Date getCheckInAt();
    Date getCheckOutAt();
    BookingStatusEnum getStatus();
    String getSpecialRequest();
    UserEntityInfo getUser();


    /**
     * Projection for {@link com.kiloit.hms.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        String getName();
    }
}