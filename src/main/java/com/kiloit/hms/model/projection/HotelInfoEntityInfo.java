package com.kiloit.hms.model.projection;

import java.time.Instant;

/**
 * Projection for {@link com.kiloit.hms.db.entity.HotelInfoEntity}
 */
public interface HotelInfoEntityInfo {
    Long getId();
    String getTitle();

    String getSubTitle();

    String getDescription();

    String getThumbnailUrl();

    UserEntityInfo getCreatedBy();
    UserEntityInfo getUpdatedBy();
    Instant getCreatedAt();
    Instant getUpdatedAt();

    /**
     * Projection for {@link com.kiloit.hms.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        Long getId();

        String getName();
    }
}