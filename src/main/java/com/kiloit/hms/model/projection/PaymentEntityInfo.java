package com.kiloit.hms.model.projection;

import com.kiloit.hms.config.enumeration.PaymentMethodEnum;
import com.kiloit.hms.config.enumeration.PaymentStatusEnum;

import java.time.Instant;
import java.util.Date;

/**
 * Projection for {@link com.kiloit.hms.db.entity.PaymentEntity}
 */
public interface PaymentEntityInfo {
    Long getId();

    double getTotalPayment();

    PaymentStatusEnum getPaymentStatus();

    PaymentMethodEnum getPaymentMethod();

    Date getPaymentDate();

    String getRemark();
}