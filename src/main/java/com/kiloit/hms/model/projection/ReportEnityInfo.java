package com.kiloit.hms.model.projection;

import com.kiloit.hms.config.enumeration.BookingStatusEnum;

/**
 * Projection for {@link com.kiloit.hms.db.entity.BookingDetailEntity}
 */
public interface ReportEnityInfo {
//    Long getId();

//    BookingEntityInfo getBooking();
    Integer getTotalBooking();
    Integer getTotalIncome();
    Integer getRoomBooked();
    String getRoomType();

    /**
     * Projection for {@link com.kiloit.hms.db.entity.RoomTypeEntity}
     */
    interface RoomTypeEntityInfo1 {
        Long getId();

        String getTitle();
    }
    interface RoomEntityInfo{
        Long getId();
        Long getRoom();
    }
}