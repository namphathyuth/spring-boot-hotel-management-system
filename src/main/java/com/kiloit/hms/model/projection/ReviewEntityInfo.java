package com.kiloit.hms.model.projection;

import java.time.Instant;

/**
 * Projection for {@link com.kiloit.hms.db.entity.ReviewEntity}
 */
public interface ReviewEntityInfo {
    Long getId();

    Instant getCreatedAt();

    Instant getUpdatedAt();

    Instant getDeletedAt();

    String getTitle();

    String getDescription();

    UserEntityInfo1 getUser();

    /**
     * Projection for {@link com.kiloit.hms.db.entity.UserEntity}
     */
    interface UserEntityInfo1 {
        String getUsername();

        String getEmail();

        String getPhone();
    }
}