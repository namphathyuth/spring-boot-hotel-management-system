package com.kiloit.hms.model.projection;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Projection for {@link com.kiloit.hms.db.entity.RoleEntity}
 */
public interface RoleEntityInfo {
    Long getId();

    String getName();

    String getCode();

}