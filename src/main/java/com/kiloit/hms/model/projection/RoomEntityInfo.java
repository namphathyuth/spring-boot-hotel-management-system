package com.kiloit.hms.model.projection;

import com.kiloit.hms.config.RoomStatusEnum;

import java.time.Instant;

/**
 * Projection for {@link com.kiloit.hms.db.entity.RoomEntity}
 */
public interface RoomEntityInfo {
    Long getId();
    String getFloor();

    String getRoom();

    RoomStatusEnum getStatus();

    UserEntityInfo getCreatedBy();
    UserEntityInfo getUpdatedBy();
    Instant getCreatedAt();
    Instant getUpdatedAt();

    RoomTypeEntityInfo1 getRoomType();

    /**
     * Projection for {@link com.kiloit.hms.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        Long getId();

        String getName();
    }

    /**
     * Projection for {@link com.kiloit.hms.db.entity.RoomTypeEntity}
     */
    interface RoomTypeEntityInfo1 {
        Long getId();
        String getTitle();
    }
}