package com.kiloit.hms.model.projection;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.util.List;

/**
 * Projection for {@link com.kiloit.hms.db.entity.RoomTypeEntity}
 */
public interface RoomTypeEntityInfo {
    Long getId();
    Instant getCreatedAt();

    Instant getUpdatedAt();

    String getTitle();

    String getSubTitle();

    String getDescription();

    int getBed();

    int getAdult();

    int getChildren();

    double getPrice();

    String getAmenity();

    UserEntityInfo getCreatedBy();
    UserEntityInfo getUpdatedBy();

    @JsonProperty("image")
    List<RoomTypeImageEntityInfo> getImage();

    /**
     * Projection for {@link com.kiloit.hms.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        Long getId();

        String getName();
    }

    /**
     * Projection for {@link com.kiloit.hms.db.entity.RoomTypeImageEntity}
     */
    interface RoomTypeImageEntityInfo {
        Long getId();
        String getUrl();
    }
}