package com.kiloit.hms.model.projection;

import java.time.Instant;

/**
 * Projection for {@link com.example.demo.db.entity.UserEntity}
 */
public interface UserEntityInfo {
    Long getId();

    String getUsername();

    String getEmail();

    String getName();

    String getBio();

    String getAvatar();

    String getAddress();

    String getPhone();

    Boolean getStatus();

    RoleEntityInfo getRoleEntity();

    UserCreatedByEntityInfo getCreatedBy();

    /**
     * Projection for {@link com.example.demo.db.entity.RoleEntity}
     */
    interface RoleEntityInfo {
        Long getId();

        String getName();

        String getCode();
    }

    /**
     * Projection for {@link com.example.demo.db.entity.UserEntity}
     */
    interface UserCreatedByEntityInfo {
        Long getId();

        String getName();
    }
}