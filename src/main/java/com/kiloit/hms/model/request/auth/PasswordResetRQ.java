package com.kiloit.hms.model.request.auth;

import lombok.Data;

@Data
public class PasswordResetRQ {
    private String oldPassword;
    private String password;
    private String confirmPassword;
}
