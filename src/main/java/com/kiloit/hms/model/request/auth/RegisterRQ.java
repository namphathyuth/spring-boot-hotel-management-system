package com.kiloit.hms.model.request.auth;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class RegisterRQ {

    @NotBlank(message = "Username is required")
    private String username;
    @NotEmpty(message = "email is required")
    private String email;
    @NotEmpty(message = "password is required")
    private String password;
    @NotEmpty(message = "name nut be empty")
    private String name;
    @NotEmpty(message = "bio nut be empty")
    private String bio;
    @NotEmpty(message = "address nut be empty")
    private String address;
    @NotEmpty(message = "phone nut be empty")
    private String phone;
}
