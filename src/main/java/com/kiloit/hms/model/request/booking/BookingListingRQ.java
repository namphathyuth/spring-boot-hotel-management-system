package com.kiloit.hms.model.request.booking;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import org.hibernate.query.sql.internal.ParameterRecognizerImpl;
import org.springframework.boot.context.properties.bind.Name;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.Date;

@Data
public class BookingListingRQ extends BaseListingRQ {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date checkIn = null;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date checkOut = null;
    private Long roomType;
    private String username;
    private BookingStatusEnum status;
}
