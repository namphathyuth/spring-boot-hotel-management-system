package com.kiloit.hms.model.request.booking;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.Date;
import java.util.List;

public record BookingRQ(
        Long userId,

        @JsonFormat(pattern = "yyyy-MM-dd")
        @NotNull(message = "Check In Date Must Be Provided")
        Date checkIn,

        @JsonFormat(pattern = "yyyy-MM-dd")
        @NotNull(message = "Check Out Date Must Be Provided")
        @Future
        Date checkOut,

        String specialRequest,

        @NotNull(message = "Booking Detail Must Not Be Null")
        List<@Valid BookingDetailRQ> bookingDetail

) {
}
