package com.kiloit.hms.model.request.booking;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public record UpdateBookingDetailRQ(
        @NotNull(message = "Room type ID must not be provided")
        Long roomTypeId,
        Long roomId,
        @NotNull(message = "Number of adult cannot be null")
        @Min(value = 1, message = "Number of adult must be at least 1")
        int adult,
        @NotNull(message = "Number of children cannot be null")
        int child

) {
}
