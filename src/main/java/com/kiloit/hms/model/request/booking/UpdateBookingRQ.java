package com.kiloit.hms.model.request.booking;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import com.kiloit.hms.exception.anotation.EnumNamePattern;
import com.kiloit.hms.exception.anotation.FieldsValueMatch;
import com.kiloit.hms.exception.anotation.Include;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.Date;

public record UpdateBookingRQ(
        @JsonFormat(pattern = "yyyy-MM-dd")
        @NotNull(message = "Check In Date Must Be Provided")
        Date checkIn,

        @JsonFormat(pattern = "yyyy-MM-dd")
        @NotNull(message = "Check Out Date Must Be Provided")
        @Future
        Date checkOut,
        String specialRequest,

        @NotNull(message = "Status must be provide")
        BookingStatusEnum status
) {
}
