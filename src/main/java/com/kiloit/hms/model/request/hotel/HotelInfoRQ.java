package com.kiloit.hms.model.request.hotel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class HotelInfoRQ {
    @JsonProperty("title")
    private String title;
    @JsonProperty("sub_title")
    private String subTitle;
    @JsonProperty("description")
    private String description;
    @JsonProperty("thumbnail_url")
    private String thumnailUrl;
}
