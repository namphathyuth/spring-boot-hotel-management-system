package com.kiloit.hms.model.request.hotel;

import lombok.Data;

@Data
public class ReviewRQ {
    private String title;
    private String description;
    private Long userId;
}
