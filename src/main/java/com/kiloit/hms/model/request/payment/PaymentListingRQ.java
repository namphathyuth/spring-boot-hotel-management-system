package com.kiloit.hms.model.request.payment;

import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.config.enumeration.PaymentStatusEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class PaymentListingRQ extends BaseListingRQ {
    private String username;
    private PaymentStatusEnum status;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date paymentDate;
}
