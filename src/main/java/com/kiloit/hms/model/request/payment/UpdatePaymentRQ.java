package com.kiloit.hms.model.request.payment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiloit.hms.config.enumeration.PaymentMethodEnum;
import com.kiloit.hms.config.enumeration.PaymentStatusEnum;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public record UpdatePaymentRQ(

        @NotNull(message = "total payment must be not null")
        double totalPayment,
        @NotNull
        PaymentStatusEnum paymentStatus,
        PaymentMethodEnum paymentMethod,
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        Date paymentDate,
        String remark

) {
}
