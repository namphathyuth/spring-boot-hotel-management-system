package com.kiloit.hms.model.request.role;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class PermissionRQ {
    @NotNull
    private Long permissionId;
    @NotNull
    private Boolean status;

}
