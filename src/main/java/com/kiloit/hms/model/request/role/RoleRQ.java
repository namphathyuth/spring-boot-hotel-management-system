package com.kiloit.hms.model.request.role;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

/**
 * @author Sombath
 * create at 24/1/24 4:02 PM
 */

@Data
public class RoleRQ {

    @NotBlank(message = "Role name required")
    @Pattern(regexp = "^[a-zA-Z\\s-]+$", message = "Role name can only contain letter, space and hyphen")
    private String name;

    @NotBlank(message = "Role code required")
    @Pattern(regexp = "^[a-zA-Z\\s-]+$", message = "Role code can only contain letter, space and hyphen")
    private String code;


}
