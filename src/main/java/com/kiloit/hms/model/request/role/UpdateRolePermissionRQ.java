package com.kiloit.hms.model.request.role;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class UpdateRolePermissionRQ {
    @NotNull
    private Long roleId;
    @NotEmpty
    private List<@Valid PermissionRQ> permissions;

}
