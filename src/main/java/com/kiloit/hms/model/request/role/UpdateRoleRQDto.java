package com.kiloit.hms.model.request.role;

import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class UpdateRoleRQDto {
    @Pattern(regexp = "^[a-zA-Z\\s-]+$", message = "Role name can only contain letter, space and hyphen")
    private String name;

    @Pattern(regexp = "^[a-zA-Z\\s-]+$", message = "Role code can only contain letter, space and hyphen")
    private String code;
}
