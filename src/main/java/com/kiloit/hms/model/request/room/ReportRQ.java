package com.kiloit.hms.model.request.room;

import com.kiloit.hms.base.BaseListingRQ;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;


@Data
public class ReportRQ extends BaseListingRQ {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
}
