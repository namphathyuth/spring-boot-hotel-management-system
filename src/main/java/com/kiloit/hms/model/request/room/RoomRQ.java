package com.kiloit.hms.model.request.room;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiloit.hms.config.RoomStatusEnum;
import lombok.Data;

@Data
public class RoomRQ {
    @JsonProperty("floor")
    private String floor;
    @JsonProperty("room")
    private String room;
    @JsonProperty("status")
    private RoomStatusEnum status;
    @JsonProperty("room_type_id")
    private Long roomTypeId;
}
