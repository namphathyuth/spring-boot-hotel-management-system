package com.kiloit.hms.model.request.room;

import com.kiloit.hms.base.BaseListingRQ;
import lombok.*;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class RoomTypeListingRQ extends BaseListingRQ {
    private int bed;
    private int adult;
    private String amenity;
    public boolean hasBed(){
        if (bed == 0) return Boolean.FALSE;
        return Boolean.TRUE;
    }
    public boolean hasAdult(){
        if(adult == 0) return Boolean.FALSE;
        return Boolean.TRUE;
    }
    public boolean hasAmenity(){
        if (amenity == null) return Boolean.FALSE;
        return Boolean.TRUE;
    }
}
