package com.kiloit.hms.model.request.user;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UpdateUserRQ {
    @NotBlank(message = "Username is required")
    private String username;
    @NotBlank(message = "email is required")
    private String email;
    @NotBlank(message = "name is required")
    private String name;
    @NotBlank(message = "bio is required")
    private String bio;
    @NotBlank(message = "address is required")
    private String address;
    @NotBlank(message = "phone is required")
    private String phone;
}
