package com.kiloit.hms.model.request.user;

import jakarta.validation.constraints.Email;
import lombok.Data;

@Data
public class UpdateUserRQDto {
    private String username;
    @Email
    private String email;
    private String name;
    private String bio;
    private String address;
    private String phone;
    private Boolean status;
    private String dateOfBirth;
    private Long roleId;
}
