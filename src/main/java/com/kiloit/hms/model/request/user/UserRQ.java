package com.kiloit.hms.model.request.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UserRQ {
    @NotBlank(message = "Username is required ")
    private String username;
    @NotBlank(message = "email is required")
    private String email;
    @NotBlank(message = "password is required")
    private String password;
    @NotBlank(message = "name is required")
    private String name;
    @NotBlank(message = "bio is required")
    private String bio;
    @NotBlank(message = "address is required")
    private String address;
    @NotBlank(message = "phone is required")
    private String phone;
    @NotBlank(message = "status is required")
    private String status;
    @NotBlank(message = "date_of_birth is required")
    private String dateOfBirth;
    @NotNull(message ="role is required")
    private Long roleId;
}
