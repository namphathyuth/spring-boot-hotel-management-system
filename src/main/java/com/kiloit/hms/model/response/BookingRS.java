package com.kiloit.hms.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiloit.hms.model.projection.BookingDetailEntityInfo;
import com.kiloit.hms.model.projection.BookingEntityInfo;
import com.kiloit.hms.model.projection.PaymentEntityInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import java.util.List;

public record BookingRS(
        BookingEntityInfo booking,
        @JsonProperty("booking_details")
        List<BookingDetailEntityInfo> bookingDetails,
        PaymentEntityInfo payment
) {
}
