package com.kiloit.hms.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import com.kiloit.hms.config.enumeration.PaymentMethodEnum;
import com.kiloit.hms.config.enumeration.PaymentStatusEnum;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * DTO for {@link com.kiloit.hms.db.entity.BookingEntity}
 */
public record BookingResponse(Long id,
                              UserEntityDto user,
                              Date checkInAt,

                              Date checkOutAt,
                              BookingStatusEnum status,

                              String specialRequest,

                              List<BookingDetailEntityDto> bookingDetail,
                              PaymentEntityDto payment) implements Serializable {
    /**
     * DTO for {@link com.kiloit.hms.db.entity.UserEntity}
     */
    public record UserEntityDto(Long id, @Size(max = 100) String name) implements Serializable {
    }

    /**
     * DTO for {@link com.kiloit.hms.db.entity.BookingDetailEntity}
     */
    public record BookingDetailEntityDto(Long id,
                                         RoomTypeEntityDto roomType,
                                         RoomEntityDto room,
                                         int adult,
                                         int child) implements Serializable {
        /**
         * DTO for {@link com.kiloit.hms.db.entity.RoomTypeEntity}
         */
        public record RoomTypeEntityDto(@Size(max = 100) String title) implements Serializable {
        }

        /**
         * DTO for {@link com.kiloit.hms.db.entity.RoomEntity}
         */
        public record RoomEntityDto(String floor, String room) implements Serializable {
        }
    }

    /**
     * DTO for {@link com.kiloit.hms.db.entity.PaymentEntity}
     */
    public record PaymentEntityDto(Long id, double totalPayment, PaymentStatusEnum paymentStatus) implements Serializable {
    }
}