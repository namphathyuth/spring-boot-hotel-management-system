package com.kiloit.hms.model.response;

import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import jakarta.validation.constraints.Size;
import lombok.Value;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * DTO for {@link com.kiloit.hms.db.entity.BookingEntity}
 */
@Value
public class CurrentUserBookingDTO implements Serializable {
    Long id;
    UserEntityDto user;
    Date checkInAt;
    Date checkOutAt;
    BookingStatusEnum status;
    String specialRequest;
    List<BookingDetailEntityDto> bookingDetail;

    /**
     * DTO for {@link com.kiloit.hms.db.entity.UserEntity}
     */
    @Value
    public static class UserEntityDto implements Serializable {
        @Size(max = 100)
        String name;
    }

    /**
     * DTO for {@link com.kiloit.hms.db.entity.BookingDetailEntity}
     */
    @Value
    public static class BookingDetailEntityDto implements Serializable {
        RoomTypeEntityDto roomType;

        /**
         * DTO for {@link com.kiloit.hms.db.entity.RoomTypeEntity}
         */
        @Value
        public static class RoomTypeEntityDto implements Serializable {
            @Size(max = 100)
            String title;
            List<RoomTypeImageEntityDto> image;

            /**
             * DTO for {@link com.kiloit.hms.db.entity.RoomTypeImageEntity}
             */
            @Value
            public static class RoomTypeImageEntityDto implements Serializable {
                String url;
            }
        }
    }
}