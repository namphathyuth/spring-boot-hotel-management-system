package com.kiloit.hms.model.response;

import com.kiloit.hms.db.entity.PermissionEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PermissionRS {
    private Long id;
    private String name;
    private String module;
    private boolean status;
}
