package com.kiloit.hms.model.response;

import com.kiloit.hms.db.entity.RoleEntity;
import com.kiloit.hms.model.projection.RoleEntityInfo;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class RoleRS {
    private RoleEntityInfo role;
    private List<PermissionRS> permissions;
}
