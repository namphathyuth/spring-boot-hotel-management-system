package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.RoleEntity;
import com.kiloit.hms.db.entity.UserEntity;
import com.kiloit.hms.db.reposiroty.RoleRepository;
import com.kiloit.hms.db.reposiroty.UserRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.model.request.auth.LoginRQ;
import com.kiloit.hms.model.request.auth.RegisterRQ;
import com.kiloit.hms.security.UserPrincipal;
import com.kiloit.hms.utils.TokenUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Sombath
 * create at 26/1/24 2:08 AM
 */

@Service
@RequiredArgsConstructor
public class AuthService extends BaseService {

    private final AuthenticationManager authenticationManager;
    private final TokenUtils tokenUtils;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public StructureRS login(LoginRQ request) {
//        System.out.println(passwordEncoder.encode(request.getPassword()));
        System.out.println(passwordEncoder.encode(request.getPassword()));

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        Authentication auth = authenticationManager.authenticate(authenticationToken);

        UserPrincipal userPrincipal = (UserPrincipal) auth.getPrincipal();

        if (!userPrincipal.getStatus())
            throw new BadRequestException(MessageConstant.AUTH.ACCOUNT_DEACTIVATE);

        Map<String, Object> respond = new HashMap<>();

        respond.put("user", userPrincipal);
        respond.put("token", tokenUtils.generateToken(userPrincipal));

        return response(respond);
    }

    public StructureRS register(RegisterRQ registerRQ) {

        Optional<RoleEntity> role = roleRepository.findById(2L);
        if (userRepository.existsByEmailIgnoreSoftDelete(registerRQ.getEmail()) > 0){
            throw new BadRequestException(MessageConstant.USERS.EMAIL_EXISTED);
        }
        if (userRepository.existsByUsernameIgnoreSoftDelete(registerRQ.getUsername()) > 0){
            throw new BadRequestException(MessageConstant.USERS.USERNAME_EXISTED);
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(registerRQ.getUsername());
        userEntity.setEmail(registerRQ.getEmail());
        userEntity.setPassword(this.passwordEncoder.encode(registerRQ.getPassword()));
        userEntity.setPhone(registerRQ.getPhone());
        userEntity.setAddress(registerRQ.getAddress());
        userEntity.setName(registerRQ.getName());
        userEntity.setBio(registerRQ.getBio());
        userEntity.setStatus(Boolean.TRUE);
        userEntity.setRoleEntity(role.get());
        userRepository.save(userEntity);
        return response(MessageConstant.AUTH.USER_REGISTERED);
    }



}
