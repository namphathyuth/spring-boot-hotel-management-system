package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.config.RoomStatusEnum;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.BookingDetailEntity;
import com.kiloit.hms.db.entity.BookingEntity;
import com.kiloit.hms.db.entity.RoomEntity;
import com.kiloit.hms.db.entity.RoomTypeEntity;
import com.kiloit.hms.db.reposiroty.BookingDetailRepository;
import com.kiloit.hms.db.reposiroty.BookingRepository;
import com.kiloit.hms.db.reposiroty.RoomRepository;
import com.kiloit.hms.db.reposiroty.RoomTypeRepository;
import com.kiloit.hms.exception.anotation.FieldsValueMatch;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.exception.httpstatus.NotFoundException;
import com.kiloit.hms.model.projection.BookingDetailEntityInfo;
import com.kiloit.hms.model.projection.BookingEntityInfo;
import com.kiloit.hms.model.request.booking.BookingDetailRQ;
import com.kiloit.hms.model.request.booking.UpdateBookingDetailRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookingDetailService extends BaseService {
    private final BookingDetailRepository bookingDetailRepository;
    private final BookingRepository bookingRepository;
    private final RoomTypeRepository roomTypeRepository;
    private final RoomRepository roomRepository;
    private final PaymentService paymentService;
    private final BookingService bookingService;

    public StructureRS getBookingDetailByBookingId(Long bookingId){
        List<BookingDetailEntityInfo> bookingDetailEntityInfos = bookingDetailRepository.getBookingDetailEntitiesByBookingId(bookingId);
        return response(bookingDetailEntityInfos);
    }

    public StructureRS createBookingDetail(Long bookingId, BookingDetailRQ request){
        Optional<BookingEntity> booking = bookingRepository.findById(bookingId);
        if (booking.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_NOT_FOUND);
        Optional<RoomTypeEntity> roomType = roomTypeRepository.findById(request.roomTypeId());
        if (roomType.isEmpty()) throw new NotFoundException(MessageConstant.ROOM.ROOM_TYPE_NOT_FOUND);
        if(!bookingService.roomAvailable(request.roomTypeId(), booking.get().getCheckInAt(), booking.get().getCheckOutAt()))
            throw new BadRequestException(MessageConstant.ROOM.ROOM_NOT_AVAILABLE);
        BookingDetailEntity bookingDetail = new BookingDetailEntity();
        bookingDetail.setBooking(booking.get());
        bookingDetail.setRoomType(roomType.get());
        bookingDetail.setAdult(request.adult());
        bookingDetail.setChild(request.child());
        bookingDetailRepository.save(bookingDetail);
        booking.get().getBookingDetail().add(bookingDetail);
        booking.get().getPayment().setTotalPayment(paymentService.calcTotalPayment(booking.get()));
        bookingRepository.save(booking.get());
        return response(HttpStatus.CREATED, MessageConstant.BOOKING.BOOKING_DETAIL_CREATED);
    }

    public StructureRS deleteBookingDetailById(Long id){
        Optional<BookingDetailEntity> bookingDetail = bookingDetailRepository.findById(id);
        if(bookingDetail.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_DETAIL_NOT_FOUND);
        BookingEntity booking = bookingDetail.get().getBooking();
        bookingDetailRepository.deleteById(id);
        booking.getPayment().setTotalPayment(paymentService.calcTotalPayment(booking));
        bookingRepository.save(booking);
        return response(HttpStatus.OK, MessageConstant.BOOKING.BOOKING_DETAIL_DELETED);
    }

    public StructureRS updateBookingDetail(Long id, UpdateBookingDetailRQ request){
        Optional<BookingDetailEntity> bookingDetail = bookingDetailRepository.findById(id);
        if (bookingDetail.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_DETAIL_NOT_FOUND);
        if (!request.roomTypeId().equals(bookingDetail.get().getRoomType().getId())) {
            if(!bookingService.roomAvailable(
                    request.roomTypeId(),
                    bookingDetail.get().getBooking().getCheckInAt(),
                    bookingDetail.get().getBooking().getCheckOutAt()
            )) throw new BadRequestException(MessageConstant.ROOM.ROOM_NOT_AVAILABLE);
            bookingDetail.get().setRoomType(roomTypeRepository.getReferenceById(request.roomTypeId()));
        }
        if (request.roomId() != null && !request.roomId().equals(
                bookingDetail.get().getRoom().getId()
        )){
            Optional<RoomEntity> room = roomRepository.findById(request.roomId());
            if (room.isEmpty()) throw new NotFoundException(MessageConstant.ROOM.ROOM_NOT_FOUND);
            else if(room.get().getStatus() != RoomStatusEnum.AVAILABLE) throw new BadRequestException(MessageConstant.ROOM.ROOM_NOT_AVAILABLE);
            bookingDetail.get().setRoom(room.get());
        }
        bookingDetail.get().setAdult(request.adult());
        bookingDetail.get().setChild(request.child());
        bookingDetail = Optional.of(bookingDetailRepository.save(bookingDetail.get()));
        BookingEntity booking = bookingDetail.get().getBooking();
        booking.getPayment().setTotalPayment(paymentService.calcTotalPayment(booking));
        bookingRepository.save(booking);
        return response(HttpStatus.OK, MessageConstant.BOOKING.BOOKING_DETAIL_UPDATED);
    }

    public StructureRS getBookingDetailById(Long id){
        Optional<BookingDetailEntityInfo> bookingDetail = bookingDetailRepository.getBookingDetailById(id);
        if (bookingDetail.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_DETAIL_NOT_FOUND);
        return response(bookingDetail.get());
    }

}
