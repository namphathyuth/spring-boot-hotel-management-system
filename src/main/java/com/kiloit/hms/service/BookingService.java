package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.config.RoomStatusEnum;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import com.kiloit.hms.config.enumeration.PaymentStatusEnum;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.BookingDetailEntity;
import com.kiloit.hms.db.entity.BookingEntity;
import com.kiloit.hms.db.entity.UserEntity;
import com.kiloit.hms.db.reposiroty.*;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.exception.httpstatus.ForbiddenException;
import com.kiloit.hms.exception.httpstatus.NotFoundException;
import com.kiloit.hms.model.mapper.BookingEntityMapper;
import com.kiloit.hms.model.mapper.BookingResponseMapper;
import com.kiloit.hms.model.projection.BookingEntityInfo;
import com.kiloit.hms.model.request.booking.BookingDetailRQ;
import com.kiloit.hms.model.request.booking.BookingListingRQ;
import com.kiloit.hms.model.request.booking.BookingRQ;
import com.kiloit.hms.model.request.booking.UpdateBookingRQ;
import com.kiloit.hms.model.response.CurrentUserBookingDTO;
import com.kiloit.hms.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookingService extends BaseService {
    private final BookingRepository bookingRepository;
    private final BookingDetailRepository bookingDetailRepository;
    private final UserRepository userRepository;
    private final RoomTypeRepository roomTypeRepository;
    private final PaymentService paymentService;
    private final PaymentRepository paymentRepository;
    private final BookingResponseMapper bookingResponseMapper;
    private final BookingEntityMapper bookingEntityMapper;

    public StructureRS booking(BookingRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        UserPrincipal userPrincipal = UserPrincipal.build(jwtAuthenticationToken);
        request.bookingDetail().forEach(bookingDetail -> {
            if(!roomAvailable(bookingDetail.roomTypeId(), request.checkIn(), request.checkOut())) {
                throw new BadRequestException(
                        MessageConstant.ROOM.ROOM_NOT_AVAILABLE);
            }
        });
        BookingEntity booking = new BookingEntity();
//        if (userPrincipal.getRoleId() == 2)
//            booking.setUser(userRepository.getReferenceById(userPrincipal.getId()));
//        else booking.setUser(userRepository.getReferenceById(request.userId()));

        if(request.userId() == null)
            booking.setUser(userRepository.findById(userPrincipal.getId()).orElseThrow(() -> new NotFoundException(MessageConstant.USERS.USER_NOT_FOUND)));
        else booking.setUser(userRepository.findById(request.userId()).orElseThrow(() -> new NotFoundException(MessageConstant.USERS.USER_NOT_FOUND)));
        booking.setCheckInAt(request.checkIn());
        booking.setCheckOutAt(request.checkOut());
        booking.setStatus(BookingStatusEnum.CONFIRMED);
        if (!request.specialRequest().isBlank()) booking.setSpecialRequest(request.specialRequest());
        booking = bookingRepository.save(booking);
        for (BookingDetailRQ bookingDetailRQ : request.bookingDetail()){
            BookingDetailEntity bookingDetail = new BookingDetailEntity();
            bookingDetail.setBooking(booking);
            bookingDetail.setRoomType(roomTypeRepository.getReferenceById(bookingDetailRQ.roomTypeId()));
            bookingDetail.setAdult(bookingDetailRQ.adult());
            bookingDetail.setChild(bookingDetailRQ.child());
            booking.getBookingDetail().add(bookingDetail);
            bookingDetailRepository.save(bookingDetail);
            bookingRepository.save(booking);
        }
        paymentService.createPayment(booking);
        return response(HttpStatus.CREATED, MessageConstant.BOOKING.BOOKING_SUCCESS);
    }

    public StructureRS getBooking(BookingListingRQ request){
        Page<BookingEntityInfo> bookingInfo = bookingRepository.getAllBookingWithFilter(
                request.getCheckIn(),
                request.getCheckOut(),
                request.getUsername(),
                request.getStatus(),
                request.getPageable(request.getSort(), request.getOrder()));
        System.out.println(bookingInfo.getContent());
        return response(bookingInfo.getContent(), bookingInfo);
    }

    public StructureRS getBookingById(Long id){
//        Optional<BookingEntityInfo> booking = bookingRepository.getBookingById(id);
//        if (booking.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_NOT_FOUND);
//
//        List<BookingDetailEntityInfo> bookingDetail = bookingDetailRepository.getBookingDetailEntitiesByBookingId(id);
//        Optional<PaymentEntityInfo> payment = paymentRepository.getByBookingId(id);
//
//        return response(new BookingRS(booking.get(), bookingDetail, payment.get()));
        BookingEntity booking = bookingRepository.findById(id).orElseThrow(() -> new NotFoundException(MessageConstant.BOOKING.BOOKING_NOT_FOUND));
        return response(bookingResponseMapper.toDto(booking));
    }

    public StructureRS getCurrentUserBooking(BookingListingRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        UserPrincipal userPrincipal = UserPrincipal.build(jwtAuthenticationToken);
        Page<BookingEntity> bookings = bookingRepository.getAllBookingEntityWithFilter(
                request.getCheckIn(),
                request.getCheckOut(),
                userPrincipal.getUsername(),
                request.getStatus(),
                request.getPageable(request.getSort(), request.getOrder())
        );

        Page<CurrentUserBookingDTO> currentUserBookingDTOPage = bookings.map(bookingEntityMapper::toDto);
        return response(currentUserBookingDTOPage.get(), currentUserBookingDTOPage);
    }

    public StructureRS deleteBookingById(Long id){
        bookingRepository.deleteById(id);
        return response(HttpStatus.OK, MessageConstant.BOOKING.BOOKING_DELETED);
    }

    public StructureRS updateBooking(Long id, UpdateBookingRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        UserPrincipal userPrincipal = UserPrincipal.build(jwtAuthenticationToken);
        Optional<BookingEntity> booking = bookingRepository.findById(id);
        if(booking.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_NOT_FOUND);
        if(!booking.get().getUser().getId().equals(userPrincipal.getId()) &&
            userPrincipal.getRoleId() == 2) throw new ForbiddenException(MessageConstant.FORBIDDEN, null);
        List<BookingDetailEntity> bookingDetail = booking.get().getBookingDetail();
        bookingDetail.forEach(bd -> {
            if(!roomAvailable(bd.getRoomType().getId(), request.checkIn(), request.checkOut())) {
                throw new BadRequestException(
                        MessageConstant.ROOM.ROOM_NOT_AVAILABLE);
            }
        });
        booking.get().setCheckInAt(request.checkIn());
        booking.get().setCheckOutAt(request.checkOut());
        booking.get().setSpecialRequest(request.specialRequest());
        booking.get().setStatus(request.status());
        booking.get().getPayment().setTotalPayment(paymentService.calcTotalPayment(booking.get()));
        bookingRepository.save(booking.get());
        return response(HttpStatus.OK, MessageConstant.BOOKING.BOOKING_UPDATED);
    }

    @Transactional
    public StructureRS editBooking(Long id, BookingRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        UserPrincipal userPrincipal = UserPrincipal.build(jwtAuthenticationToken);
        BookingEntity booking = bookingRepository.findById(id).orElseThrow(() -> new NotFoundException(MessageConstant.BOOKING.BOOKING_NOT_FOUND));
        if(!booking.getUser().getId().equals(userPrincipal.getId()) && userPrincipal.getRoleId() == 2)
            throw new ForbiddenException(MessageConstant.FORBIDDEN, null);

        bookingDetailRepository.deleteAllByBookingId(id);

        request.bookingDetail().forEach(bookingDetail -> {
            if(!roomAvailable(bookingDetail.roomTypeId(), request.checkIn(), request.checkOut())) {
                throw new BadRequestException(
                        MessageConstant.ROOM.ROOM_NOT_AVAILABLE);
            }
        });

        if (userPrincipal.getRoleId() != 2){
            UserEntity user = userRepository.findById(request.userId()).orElseThrow(() -> new NotFoundException(MessageConstant.USERS.USER_NOT_FOUND));
            booking.setUser(user);
        }
        booking.setCheckInAt(request.checkIn());
        booking.setCheckOutAt(request.checkOut());
        booking.setSpecialRequest(request.specialRequest());

        request.bookingDetail().forEach(bd -> {
            BookingDetailEntity bookingDetail = new BookingDetailEntity();
            bookingDetail.setBooking(booking);
            bookingDetail.setRoomType(roomTypeRepository.getReferenceById(bd.roomTypeId()));
            bookingDetail.setAdult(bd.adult());
            bookingDetail.setChild(bd.child());
            booking.getBookingDetail().add(bookingDetail);
            bookingDetailRepository.save(bookingDetail);
        });
        booking.getPayment().setTotalPayment(paymentService.calcTotalPayment(booking));
        bookingRepository.save(booking);
        return response(HttpStatus.OK, MessageConstant.BOOKING.BOOKING_UPDATED);
    }

    public StructureRS cancelBooking(Long id, JwtAuthenticationToken jwtAuthenticationToken){
        UserPrincipal userPrincipal = UserPrincipal.build(jwtAuthenticationToken);
        Optional<BookingEntity> booking = bookingRepository.findById(id);
        if(booking.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_NOT_FOUND);
        if (!booking.get().getUser().getId().equals(userPrincipal.getId()) &&
            userPrincipal.getRoleId() == 2) throw new ForbiddenException(MessageConstant.FORBIDDEN, null);
        if(booking.get().getStatus() == BookingStatusEnum.CHECKED_IN || booking.get().getStatus() == BookingStatusEnum.CHECKED_OUT)
            throw new BadRequestException(MessageConstant.BOOKING.CANCEL_NOT_ALLOWED);
        booking.get().setStatus(BookingStatusEnum.CANCELLED);
        booking.get().getPayment().setPaymentStatus(PaymentStatusEnum.CANCELLED);
        bookingRepository.save(booking.get());
        return response(HttpStatus.OK, MessageConstant.BOOKING.BOOKING_CANCELLED);
    }
    boolean roomAvailable(Long roomTypeId, Date checkIn, Date checkOut){
        List<RoomStatusEnum> roomStatus = Arrays.asList(
                RoomStatusEnum.MAINTENANCE,
                RoomStatusEnum.RESERVED);
        int totalBook = bookingDetailRepository.countAllBookingDetailBetween(roomTypeId, checkIn, checkOut);
        System.out.println(totalBook);
        int totalRoom = bookingDetailRepository.countAllRoomByRoomTypeWhereStatusNotIn(roomTypeId, roomStatus);
        System.out.println(totalRoom);
        if(totalBook >= totalRoom){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

}
