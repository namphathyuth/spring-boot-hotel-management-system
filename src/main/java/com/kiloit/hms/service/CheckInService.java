package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.config.RoomStatusEnum;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.BookingEntity;
import com.kiloit.hms.db.entity.RoomEntity;
import com.kiloit.hms.db.entity.RoomTypeEntity;
import com.kiloit.hms.db.reposiroty.BookingDetailRepository;
import com.kiloit.hms.db.reposiroty.BookingRepository;
import com.kiloit.hms.db.reposiroty.RoomRepository;
import com.kiloit.hms.db.reposiroty.RoomTypeRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.exception.httpstatus.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CheckInService extends BaseService {
    private final BookingRepository bookingRepository;
    private final RoomRepository roomRepository;
    private final RoomTypeRepository roomTypeRepository;
    private final BookingDetailRepository bookingDetailRepository;
    public StructureRS checkIn(Long bookingId){
        Optional<BookingEntity> booking = bookingRepository.findById(bookingId);
        if (booking.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_NOT_FOUND);
        booking.get().getBookingDetail().forEach(bd -> {
            RoomTypeEntity roomType = roomTypeRepository.getReferenceById(bd.getRoomType().getId());
            Optional<RoomEntity> room = roomRepository.getFirstByRoomTypeAndStatusIs(roomType, RoomStatusEnum.AVAILABLE);
            if (room.isEmpty()) throw new BadRequestException(MessageConstant.ROOM.ROOM_NOT_AVAILABLE);
            bd.setRoom(room.get());
            room.get().setStatus(RoomStatusEnum.OCCUPIED);
            roomRepository.save(room.get());
            bookingDetailRepository.save(bd);
        });
        booking.get().setStatus(BookingStatusEnum.CHECKED_IN);
        bookingRepository.save(booking.get());
        return response(HttpStatus.OK, MessageConstant.BOOKING.CHECKED_IN);
    }
}
