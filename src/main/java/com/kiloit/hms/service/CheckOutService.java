package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.config.RoomStatusEnum;
import com.kiloit.hms.config.enumeration.BookingStatusEnum;
import com.kiloit.hms.config.enumeration.PaymentStatusEnum;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.BookingEntity;
import com.kiloit.hms.db.reposiroty.BookingRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.exception.httpstatus.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CheckOutService extends BaseService {
    private final BookingRepository bookingRepository;
    public StructureRS checkOut(Long bookingId){
        Optional<BookingEntity> booking = bookingRepository.findById(bookingId);
        if(booking.isEmpty()) throw new NotFoundException(MessageConstant.BOOKING.BOOKING_NOT_FOUND);
        if(!booking.get().getStatus().equals(BookingStatusEnum.CHECKED_IN)) throw new BadRequestException(MessageConstant.BOOKING.CHECK_OUT_NOT_ALLOWED);
        if(booking.get().getPayment().getPaymentStatus() != PaymentStatusEnum.PAID) throw new BadRequestException(MessageConstant.PAYMENT.PAYMENT_REQUIRED);
        booking.get().setStatus(BookingStatusEnum.CHECKED_OUT);
        booking.get().getBookingDetail().forEach(bd -> {
            bd.getRoom().setStatus(RoomStatusEnum.AVAILABLE);
        });
        bookingRepository.save(booking.get());
        return response();
    }
}
