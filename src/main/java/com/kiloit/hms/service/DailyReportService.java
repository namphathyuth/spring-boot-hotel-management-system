package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.reposiroty.DailyReportRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.model.projection.ReportEnityInfo;
import com.kiloit.hms.model.request.room.ReportRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class DailyReportService extends BaseService {
    private final DailyReportRepository dailyReportRepository;
    public StructureRS getBooking(ReportRQ reportRQ) {
        if (reportRQ.getStartDate() == null && reportRQ.getEndDate() == null){
            Page<ReportEnityInfo> booking = dailyReportRepository.findDailyReportEntitiesByTotalBooking(new Date(), new Date(), reportRQ.getPageable());
            return response(booking.getContent(), booking);
        }
        Page<ReportEnityInfo> booking = dailyReportRepository.findDailyReportEntitiesByTotalBooking(reportRQ.getStartDate(), reportRQ.getEndDate(), reportRQ.getPageable());
        return response(booking.getContent(), booking);
    }
    public StructureRS getIncome(ReportRQ reportRQ) {
        if (reportRQ.getStartDate() == null && reportRQ.getEndDate() == null){
            Page<ReportEnityInfo> income = dailyReportRepository.findDailyReportEntitiesByTotalIncome(new Date(), new Date(), reportRQ.getPageable());
            return response(income.getContent(), income);
        }
        Page<ReportEnityInfo> income = dailyReportRepository.findDailyReportEntitiesByTotalIncome(reportRQ.getStartDate(), reportRQ.getEndDate(), reportRQ.getPageable());
        return response(income.getContent(), income);
    }
    public StructureRS getRoomBooked(ReportRQ reportRQ) {
        if (reportRQ.getStartDate() == null && reportRQ.getEndDate() == null){
            Page<ReportEnityInfo> roomBooked = dailyReportRepository.findDailyReportEntitiesByRoomBooked(new Date(), new Date(), reportRQ.getPageable());
            return response(roomBooked.getContent(), roomBooked);
        }
        Page<ReportEnityInfo> roomBooked = dailyReportRepository.findDailyReportEntitiesByRoomBooked(reportRQ.getStartDate(), reportRQ.getEndDate(), reportRQ.getPageable());
        return response(roomBooked.getContent(), roomBooked);
    }
}
