package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.HotelInfoEntity;
import com.kiloit.hms.db.reposiroty.HotelInfoRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.model.projection.HotelInfoEntityInfo;
import com.kiloit.hms.model.request.hotel.HotelInfoRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HotelInfoService extends BaseService {
    private final HotelInfoRepository hotelInfoRepository;
    public StructureRS getInfo(BaseListingRQ request) {
        Page<HotelInfoEntityInfo> hotelInfoEntityInfos = hotelInfoRepository.findHotelInfoEntitiesByTitle(request.getQuery(), request.getPageable(request.getSort(), request.getOrder()));
        return response(hotelInfoEntityInfos.getContent(), hotelInfoEntityInfos);
    }

    public StructureRS getById(Long id) {
        Optional<HotelInfoEntityInfo> hotelInfoEntity = Optional.ofNullable(hotelInfoRepository.findByIdInfo(id));
        if (hotelInfoEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.HOTEL.HOTEL_INFO_NOT_FOUND);
        }
        return response(hotelInfoEntity.get());
    }

    public StructureRS addInfo(HotelInfoRQ request) {
        HotelInfoEntity hotelInfo = new HotelInfoEntity();
        hotelInfo.setTitle(request.getTitle());
        hotelInfo.setSubTitle(request.getSubTitle());
        hotelInfo.setDescription(request.getDescription());
        hotelInfo.setThumbnailUrl(request.getThumnailUrl());
        hotelInfoRepository.save(hotelInfo);
        return response();
    }

    public StructureRS editInfo(HotelInfoRQ request, Long id) {
        Optional<HotelInfoEntity> hotelInfoEntity = hotelInfoRepository.findById(id);
        if (hotelInfoEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.HOTEL.HOTEL_INFO_NOT_FOUND);
        }else {
            hotelInfoEntity.get().setTitle(request.getTitle());
            hotelInfoEntity.get().setSubTitle(request.getSubTitle());
            hotelInfoEntity.get().setDescription(request.getDescription());
            hotelInfoEntity.get().setThumbnailUrl(request.getThumnailUrl());
            hotelInfoRepository.save(hotelInfoEntity.get());
            return response();
        }
    }

    public StructureRS deleteInfo(Long id) {
        Optional<HotelInfoEntity> hotelInfoEntity = hotelInfoRepository.findById(id);
        if (hotelInfoEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.HOTEL.HOTEL_INFO_NOT_FOUND);
        }
        hotelInfoRepository.deleteById(id);
        return response();
    }
}
