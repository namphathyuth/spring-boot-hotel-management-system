package com.kiloit.hms.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.kiloit.hms.config.property.S3Buckets;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
@RequiredArgsConstructor
public class ImageUploadService {
    private final AmazonS3 amazonS3;
    private final S3Buckets s3Buckets;
    public void uploadImageToS3(String filename, File file){
        amazonS3.putObject(new PutObjectRequest(s3Buckets.getName(), filename, file).withCannedAcl(CannedAccessControlList.PublicRead));
    }
}
