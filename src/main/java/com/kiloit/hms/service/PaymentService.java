package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.config.enumeration.PaymentStatusEnum;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.BookingDetailEntity;
import com.kiloit.hms.db.entity.BookingEntity;
import com.kiloit.hms.db.entity.PaymentEntity;
import com.kiloit.hms.db.reposiroty.BookingRepository;
import com.kiloit.hms.db.reposiroty.PaymentRepository;
import com.kiloit.hms.exception.httpstatus.NotFoundException;
import com.kiloit.hms.model.projection.PaymentEntityInfo;
import com.kiloit.hms.model.request.payment.PaymentListingRQ;
import com.kiloit.hms.model.request.payment.UpdatePaymentRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PaymentService extends BaseService {
    private final BookingRepository bookingRepository;
    private final PaymentRepository paymentRepository;
    public void createPayment(BookingEntity booking){
        PaymentEntity payment = new PaymentEntity();
        payment.setBooking(booking);
        payment.setTotalPayment(calcTotalPayment(booking));
        payment.setPaymentStatus(PaymentStatusEnum.PENDING);
        paymentRepository.save(payment);
    }

    public StructureRS getPaymentByBookingId(Long bookingId){
        Optional<PaymentEntityInfo> payment = paymentRepository.getByBookingId(bookingId);
        if(payment.isEmpty()) throw new NotFoundException(MessageConstant.PAYMENT.PAYMENT_NOT_FOUNT);
        return response(payment.get());
    }

    public StructureRS getPayment(PaymentListingRQ request){
        Page<PaymentEntityInfo> payments = paymentRepository.getPaymentsWithFilter(
                request.getStatus(),
                request.getUsername(),
                request.getPaymentDate(),
                request.getPageable(request.getSort(), request.getOrder())
        );

        return response(payments.getContent(), payments);
    }

    public StructureRS deletePaymentById(Long id){
        paymentRepository.deleteById(id);
        return response(HttpStatus.OK, MessageConstant.PAYMENT.PAYMENT_DELETED);
    }
    public StructureRS getPaymentById(Long id){
        Optional<PaymentEntityInfo> payment = paymentRepository.getPaymentEntityById(id);
        if(payment.isEmpty()) throw new NotFoundException(MessageConstant.PAYMENT.PAYMENT_NOT_FOUNT);
        return response(payment.get());
    }


    public StructureRS updatePayment(Long id, UpdatePaymentRQ request){
        Optional<PaymentEntity> payment = paymentRepository.findById(id);
        if (payment.isEmpty()) throw new NotFoundException(MessageConstant.PAYMENT.PAYMENT_NOT_FOUNT);
        payment.get().setTotalPayment(request.totalPayment());
        payment.get().setPaymentStatus(request.paymentStatus());
        payment.get().setPaymentMethod(request.paymentMethod());
        payment.get().setPaymentDate(request.paymentDate());
        payment.get().setRemark(request.remark());
        paymentRepository.save(payment.get());
        return response(HttpStatus.OK, MessageConstant.PAYMENT.PAYMENT_UPDATED);
    }

    double calcTotalPayment(BookingEntity booking){
        LocalDate checkIn = booking.getCheckInAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate checkOut = booking.getCheckOutAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        long totalDays = ChronoUnit.DAYS.between(checkIn, checkOut);
        double totalPayment = 0;
        for (BookingDetailEntity bd : booking.getBookingDetail()){
            totalPayment += bd.getRoomType().getPrice() * totalDays;
        }
       return totalPayment;
    }
}
