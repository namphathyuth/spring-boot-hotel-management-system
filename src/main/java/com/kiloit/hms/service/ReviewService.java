package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.ReviewEntity;
import com.kiloit.hms.db.entity.UserEntity;
import com.kiloit.hms.db.reposiroty.ReviewEntityRepository;
import com.kiloit.hms.db.reposiroty.UserRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.model.projection.ReviewEntityInfo;
import com.kiloit.hms.model.projection.UserEntityInfo;
import com.kiloit.hms.model.request.hotel.ReviewRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReviewService extends BaseService {

    private final UserRepository userRepository;
    private final ReviewEntityRepository reviewEntityRepository;

    public StructureRS getReview(BaseListingRQ baseListingRQ) {
        Page<ReviewEntityInfo> review = reviewEntityRepository.findByTitleLike(baseListingRQ.getQuery(), baseListingRQ.getPageable(baseListingRQ.getSort(), baseListingRQ.getOrder()));
        return response(review.getContent(),review);
    }
    public StructureRS getReviewId(Long id) {
        Optional<ReviewEntityInfo> reviewEntityInfo = Optional.ofNullable(reviewEntityRepository.findByIdReview(id));
        if (reviewEntityInfo.isEmpty()){
            throw new BadRequestException(MessageConstant.REVIEW.ID_NOTFOUND);
        }
        return response(reviewEntityInfo.get());
    }
    public StructureRS addReview(@RequestBody ReviewRQ reviewRQ) {
        ReviewEntity reviewEntity = new ReviewEntity();
        reviewEntity.setTitle(reviewRQ.getTitle());
        reviewEntity.setDescription(reviewRQ.getDescription());
        reviewEntity.setUser(userRepository.getReferenceById(reviewRQ.getUserId()));
        reviewEntityRepository.save(reviewEntity);
        return response(MessageConstant.REVIEW.ADD_SUCCESS);
    }


    public StructureRS deleteReview(Long id) {
        Optional<ReviewEntity> reviewEntity = reviewEntityRepository.findById(id);
        if (reviewEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.REVIEW.ID_NOTFOUND);
        }
        reviewEntityRepository.deleteById(id);
        return response(MessageConstant.REVIEW.DELETED);
    }
}
