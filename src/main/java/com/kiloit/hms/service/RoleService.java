package com.kiloit.hms.service;


import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.PermissionEntity;
import com.kiloit.hms.db.entity.RoleEntity;
import com.kiloit.hms.db.reposiroty.PermissionEntityRepository;
import com.kiloit.hms.db.reposiroty.RoleRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.exception.httpstatus.NotFoundException;
import com.kiloit.hms.model.mapper.UpdateRoleMapper;
import com.kiloit.hms.model.projection.RoleEntityInfo;
import com.kiloit.hms.model.request.role.PermissionRQ;
import com.kiloit.hms.model.request.role.UpdateRoleRQDto;
import com.kiloit.hms.model.response.PermissionRS;
import com.kiloit.hms.model.request.role.RoleRQ;
import com.kiloit.hms.model.request.role.UpdateRolePermissionRQ;
import com.kiloit.hms.model.response.RoleRS;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Dane
 * create at 23/1/24 3:37 PM
 */

@Service
@RequiredArgsConstructor
public class RoleService extends BaseService {

    private final RoleRepository roleRepository;
    private final PermissionEntityRepository permissionEntityRepository;
    private final UpdateRoleMapper updateRoleMapper;

    public StructureRS getRoles(BaseListingRQ request) {
        Page<RoleEntityInfo> roleEntityInfoPage = roleRepository.findByNameStartsWithAndCode(request.getQuery(), request.getPageable(request.getSort(), request.getOrder()));
        return response(roleEntityInfoPage.getContent(), roleEntityInfoPage);
    }

    public StructureRS addRole(RoleRQ roleRQ) {
        if (roleRepository.existsByName(roleRQ.getName())) throw new BadRequestException(MessageConstant.ROLE.ROLE_NAME_EXISTED);
        if (roleRepository.existsByCode(roleRQ.getCode())) throw new BadRequestException(MessageConstant.ROLE.ROLE_CODE_EXISTED);

        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setName(roleRQ.getName());
        roleEntity.setCode(roleRQ.getCode());
        roleRepository.save(roleEntity);
        return response(MessageConstant.ROLE.ROLES_ADDED);
    }

    public StructureRS updateRole(UpdateRoleRQDto updateRoleRQDto, Long id) {
        RoleEntity roleEntity = roleRepository.findRoleEntityById(id).orElseThrow(() -> new NotFoundException(MessageConstant.ROLE.ROLE_ID_NOT_FOUND));
        if (updateRoleRQDto.getName() != null && roleRepository.existsByName(updateRoleRQDto.getName()))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NAME_EXISTED);
        if (updateRoleRQDto.getCode() != null && roleRepository.existsByCode(updateRoleRQDto.getCode()))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_CODE_EXISTED);
        roleEntity = updateRoleMapper.partialUpdate(updateRoleRQDto, roleEntity);
        roleRepository.save(roleEntity);
        return response(MessageConstant.ROLE.ROLE_UPDATED);
    }

    public StructureRS deleteRole(Long id) {
        Optional<RoleEntity> optionalRoleEntity = roleRepository.findRoleEntityById(id);
        if (optionalRoleEntity.isEmpty()) {
            throw new BadRequestException(MessageConstant.ROLE.ROLE_ID_NOT_FOUND);
        }
        roleRepository.deleteById(id);
        return response(MessageConstant.ROLE.ROLE_DELETED);
    }

    public StructureRS updatePermission(UpdateRolePermissionRQ updateRolePermissionRQ) {
        if (roleRepository.findRoleEntityById(updateRolePermissionRQ.getRoleId()).isEmpty())
            throw new BadRequestException(MessageConstant.ROLE.ROLE_ID_NOT_FOUND);

        RoleEntity roleEntity = roleRepository.findByIdFetchPermission(updateRolePermissionRQ.getRoleId());

        List<PermissionEntity> permissionEntities = permissionEntityRepository.findByIdIn(updateRolePermissionRQ.getPermissions().stream().map(PermissionRQ::getPermissionId).collect(Collectors.toList()));

        for (PermissionRQ permissionRQ : updateRolePermissionRQ.getPermissions()) {

            Optional<PermissionEntity> optionalPermissionEntity = permissionEntities.stream().filter(it -> it.getId().equals(permissionRQ.getPermissionId())).findFirst();

            if (optionalPermissionEntity.isPresent()) {
                PermissionEntity permissionEntity = optionalPermissionEntity.get();

                if (permissionRQ.getStatus()) {
                    if (!roleEntity.getPermissionEntities().contains(permissionEntity))
                        roleEntity.getPermissionEntities().add(permissionEntity);
                } else
                    roleEntity.getPermissionEntities().remove(permissionEntity);

            }

        }

        roleRepository.save(roleEntity);

        return response();
    }

//    public StructureRS showById(Long id) {
//        Optional<RoleEntityInfo> optionalRoleEntity = roleRepository.findByIdRole(id);
//        if (optionalRoleEntity.isEmpty()) {
//            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);
//        }
//        return response(optionalRoleEntity.get());
//    }

    public StructureRS getRoleById(Long id){
        Optional<RoleEntityInfo> role = roleRepository.findByIdRole(id);
        if (role.isEmpty()) throw new BadRequestException(MessageConstant.ROLE.ROLE_ID_NOT_FOUND);

        RoleEntity roleEntity = roleRepository.findByIdFetchPermission(id);
        List<PermissionRS> permissionRS = new ArrayList<>();
        List<PermissionEntity> allPermissionEntities = permissionEntityRepository.findAll();
        for (PermissionEntity perm : allPermissionEntities){
            permissionRS.add(new PermissionRS(perm.getId(), perm.getName(), perm.getModule(), Boolean.FALSE));
        }

        for (PermissionEntity perm : roleEntity.getPermissionEntities()){
            permissionRS.stream().filter(permRS -> permRS.getId().equals(perm.getId())).forEach(permRS -> permRS.setStatus(Boolean.TRUE));
        }

        return response(new RoleRS(role.get(), permissionRS));

    }
}