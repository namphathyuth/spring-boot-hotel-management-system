package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.RoomEntity;
import com.kiloit.hms.db.entity.RoomTypeEntity;
import com.kiloit.hms.db.reposiroty.RoomRepository;
import com.kiloit.hms.db.reposiroty.RoomTypeRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.model.projection.RoomEntityInfo;
import com.kiloit.hms.model.request.room.RoomRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoomService extends BaseService {
    private final RoomRepository roomRepository;
    private final RoomTypeRepository roomTypeRepository;
    public StructureRS getRoom(Instant startDate, Instant endDate, BaseListingRQ request) {
        Page<RoomEntityInfo> roomEntityInfo = roomRepository.findRoomEntitiesByRoom(startDate, endDate, request.getQuery(), request.getPageable(request.getSort(), request.getOrder()));
        return response(roomEntityInfo.getContent(), roomEntityInfo);
    }

    public StructureRS getById(Long id) {
        Optional<RoomEntityInfo> roomEntity = roomRepository.findByIdRoom(id);

        if (roomEntity.isEmpty())
            throw new BadRequestException(MessageConstant.ROOM.ROOM_NOT_FOUND);
        return response(roomEntity.get());
    }

    public StructureRS addRoom(RoomRQ request) {
        Optional<RoomTypeEntity> roomType = roomTypeRepository.findById(request.getRoomTypeId());
        if (roomType.isEmpty())
            throw new BadRequestException(MessageConstant.ROOM.ROOM_TYPE_NOT_FOUND);
        RoomEntity room = new RoomEntity();
        room.setRoom(request.getRoom());
        room.setFloor(request.getFloor());
        room.setStatus(request.getStatus());
        room.setRoomType(roomType.get());
        roomRepository.save(room);
        return response();
    }

    public StructureRS editRoom(Long id, RoomRQ request) {
        Optional<RoomEntity> roomEntity = roomRepository.findById(id);
        if (roomEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.ROOM.ROOM_NOT_FOUND);
        }else {
            roomEntity.get().setRoom(request.getRoom());
            roomEntity.get().setFloor(request.getFloor());
            roomEntity.get().setStatus(request.getStatus());
            roomRepository.save(roomEntity.get());
            return response();
        }
    }

    @Transactional
    public StructureRS deleteRoom(Long id) {
        if (!roomRepository.existsById(id))
            throw new BadRequestException(MessageConstant.ROOM.ROOM_NOT_FOUND);
        roomRepository.deleteById(id);

        return response();
    }
}
