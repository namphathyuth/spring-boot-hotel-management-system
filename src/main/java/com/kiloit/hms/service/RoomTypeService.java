package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.config.property.S3Buckets;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.RoomTypeEntity;
import com.kiloit.hms.db.entity.RoomTypeImageEntity;
import com.kiloit.hms.db.reposiroty.RoomTypeImageRepository;
import com.kiloit.hms.db.reposiroty.RoomTypeRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.model.projection.RoomTypeEntityInfo;
import com.kiloit.hms.model.request.room.RoomTypeListingRQ;
import com.kiloit.hms.model.request.room.RoomTypeRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RoomTypeService extends BaseService {
    private final RoomTypeRepository roomTypeRepository;
    private final RoomTypeImageRepository roomTypeImageRepository;
    private final S3Service s3Service;
    private final S3Buckets s3Buckets;
    private final ImageUploadService imageUploadService;

    public StructureRS getRoomType(RoomTypeListingRQ request) {
        Page<RoomTypeEntityInfo> roomTypeEntityInfos = roomTypeRepository.findRoomTypeEntitiesByTitle(
                request.getQuery(),
                request.getAdult(),
                request.getBed(),
                request.getAmenity(),
                request.getPageable(request.getSort(), request.getOrder()));
        return response(roomTypeEntityInfos.getContent(), roomTypeEntityInfos);
    }

    public StructureRS getById(Long id) {
        Optional<RoomTypeEntityInfo> roomTypeEntityInfo = Optional.ofNullable(roomTypeRepository.findByIdRoomType(id));

        if (roomTypeEntityInfo.isEmpty())
            throw new BadRequestException(MessageConstant.ROOM.ROOM_TYPE_NOT_FOUND);
        return response(roomTypeEntityInfo.get());
    }
//     public StructureRS createRoomType(RoomTypeRQ request, MultipartFile[] images){
//         RoomTypeEntity roomType = new RoomTypeEntity();
//         roomType.setBed(request.getBed());
//         roomType.setTitle(request.getTitle());
//         roomType.setAdult(request.getAdult());
//         roomType.setPrice(request.getPrice());
//         roomType.setAmenity(request.getAmenity());
//         roomType.setChildren(request.getChildren());
//         roomType.setSubTitle(request.getSub_title());
//         roomType.setDescription(request.getDescription());
//         roomType = roomTypeRepository.save(roomType);
//
//         if(images != null){
//             for (MultipartFile image : images){
//                 if (!image.isEmpty())
//                    addRoomTypeImg(image, roomType);
//             }
//         }
//         return response();
//     }

    public StructureRS addRoomType(RoomTypeRQ request) {
        RoomTypeEntity roomType = new RoomTypeEntity();
        roomType.setBed(request.getBed());
        roomType.setTitle(request.getTitle());
        roomType.setAdult(request.getAdult());
        roomType.setPrice(request.getPrice());
        roomType.setAmenity(request.getAmenity());
        roomType.setChildren(request.getChildren());
        roomType.setSubTitle(request.getSub_title());
        roomType.setDescription(request.getDescription());
        roomType = roomTypeRepository.save(roomType);
        ProjectionFactory projectionFactory = new SpelAwareProxyProjectionFactory();
        RoomTypeEntityInfo roomTypeEntityInfo = projectionFactory.createProjection(RoomTypeEntityInfo.class, roomType);

        return response(roomTypeEntityInfo);
    }

    public StructureRS editRoomType(Long id, RoomTypeRQ request) throws IOException {
        Optional<RoomTypeEntity> roomTypeEntity = roomTypeRepository.findById(id);

        if (roomTypeEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.ROOM.ROOM_TYPE_NOT_FOUND);
        }else {
            roomTypeEntity.get().setBed(request.getBed());
            roomTypeEntity.get().setTitle(request.getTitle());
            roomTypeEntity.get().setAdult(request.getAdult());
            roomTypeEntity.get().setPrice(request.getPrice());
            roomTypeEntity.get().setAmenity(request.getAmenity());
            roomTypeEntity.get().setChildren(request.getChildren());
            roomTypeEntity.get().setSubTitle(request.getSub_title());
            roomTypeEntity.get().setDescription(request.getDescription());
            roomTypeRepository.save(roomTypeEntity.get());

//            if (!request.getImageToRemove().isEmpty()) {
//                RoomTypeImageEntity roomTypeImage = null;
//                for (Long imgId : request.getImageToRemove()){
//                    roomTypeImage = roomTypeImageRepository.getReferenceById(imgId);
//                    Path imgPath = Path.of("src/main/resources/static/" + roomTypeImage.getUrl());
//                    File imgFile = new File(String.valueOf(imgPath));
//                    imgFile.delete();
//                }
//                request.getImageToRemove().forEach(roomTypeImageRepository::deleteById);
//            }
            return response();
        }
    }

//    public StructureRS updateRoomType(Long id, RoomTypeRQ request, MultipartFile[] images) {
//        Optional<RoomTypeEntity> roomTypeEntity = roomTypeRepository.findById(id);
//
//        if (roomTypeEntity.isEmpty()){
//            throw new BadRequestException(MessageConstant.ROOM.ROOM_TYPE_NOT_FOUND);
//        }else {
//            roomTypeEntity.get().setBed(request.getBed());
//            roomTypeEntity.get().setTitle(request.getTitle());
//            roomTypeEntity.get().setAdult(request.getAdult());
//            roomTypeEntity.get().setPrice(request.getPrice());
//            roomTypeEntity.get().setAmenity(request.getAmenity());
//            roomTypeEntity.get().setChildren(request.getChildren());
//            roomTypeEntity.get().setSubTitle(request.getSub_title());
//            roomTypeEntity.get().setDescription(request.getDescription());
//            roomTypeRepository.save(roomTypeEntity.get());
//
//            if (!request.getImageToRemove().isEmpty()) {
//                request.getImageToRemove().forEach(roomTypeImageRepository::deleteById);
//            }
//
//            if(images != null){
//                for (MultipartFile image : images){
//                    if (!image.isEmpty())
//                        addRoomTypeImg(image, roomTypeEntity.get());
//                }
//            }
//
//            return response();
//        }
//    }

    public StructureRS deleteRoomType(Long id) {
        Optional<RoomTypeEntity> roomTypeEntity = roomTypeRepository.findById(id);
        if (roomTypeEntity.isEmpty())
            throw new BadRequestException(MessageConstant.ROOM.ROOM_TYPE_NOT_FOUND);
//        List<RoomTypeImageEntity> roomTypeImage = roomTypeEntity.get().getImage();
//        for(RoomTypeImageEntity image : roomTypeImage){
//            Path imgPath = Path.of("src/main/resources/static/" + image.getUrl());
//            Boolean imgDelete = new File(String.valueOf(imgPath)).delete();
//        }
        roomTypeRepository.deleteById(id);
        return response();
    }

    public String saveImageToStorage(String uploadDirectory, MultipartFile imageFile, Long roomTypeId) throws IOException {
        Optional<RoomTypeEntity> roomType = Optional.of(roomTypeRepository.getById(roomTypeId));

        if(roomType.isEmpty())
            throw new BadRequestException(MessageConstant.ROOM.ROOM_TYPE_NOT_FOUND);

        RoomTypeImageEntity roomTypeImage = new RoomTypeImageEntity();
        String uniqueFileName = UUID.randomUUID().toString() + "_" + imageFile.getOriginalFilename();
        Path uploadPath = Path.of(uploadDirectory);
        Path filePath = uploadPath.resolve(uniqueFileName);

        if (!Files.exists(uploadPath))
            Files.createDirectories(uploadPath);
        Files.copy(imageFile.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);

        roomTypeImage.setRoomType(roomType.get());
        roomTypeImage.setUrl("image/"+uniqueFileName);
        roomTypeImageRepository.save(roomTypeImage);
        return uniqueFileName;
    }

    public StructureRS addRoomTypeImage(Long roomTypeId, MultipartFile[] images){
        RoomTypeEntity roomType = roomTypeRepository.findById(roomTypeId).orElseThrow(
                () -> new BadRequestException(MessageConstant.ROOM.ROOM_TYPE_NOT_FOUND));
        if(images != null){
             for (MultipartFile image : images){
                 if (!image.isEmpty())
                    saveRoomTypeImg(image, roomType);
             }
        }
        return response();
    }

    public StructureRS deleteRoomTypeImage(Long imageId){
        roomTypeImageRepository.deleteById(imageId);
        return response();
    }

    public void saveRoomTypeImg(MultipartFile img, RoomTypeEntity roomTypeEntity){
        if (!img.getContentType().equals("image/jpeg") &&
            !img.getContentType().equals("image/jpg") &&
            !img.getContentType().equals("image/png")) throw new BadRequestException(MessageConstant.ROOM.INVALID_IMAGE);

        if(img.getSize() > 1024*1024*5) throw new BadRequestException(MessageConstant.ROOM.UPLOAD_IMAGE_EXCEED_LIMIT_SIZE);
        String fileUrl;

        try {
            File file = s3Service.convertMultiPartToFile(img);
            String fileName = "room-type-image/" + UUID.randomUUID().toString() + s3Service.generateFileName(img);
            fileUrl = s3Buckets.getEndPointUrl() + "/" + fileName;
            imageUploadService.uploadImageToS3(fileName, file);
            file.delete();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        RoomTypeImageEntity roomTypeImage = new RoomTypeImageEntity();
        roomTypeImage.setUrl(fileUrl);
        roomTypeImage.setRoomType(roomTypeEntity);
        roomTypeEntity.getImage().add(roomTypeImage);
        roomTypeRepository.save(roomTypeEntity);
    }
}
