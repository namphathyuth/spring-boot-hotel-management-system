package com.kiloit.hms.service;

import com.kiloit.hms.base.BaseListingRQ;
import com.kiloit.hms.base.BaseService;
import com.kiloit.hms.base.StructureRS;
import com.kiloit.hms.config.property.S3Buckets;
import com.kiloit.hms.constant.MessageConstant;
import com.kiloit.hms.db.entity.UserEntity;
import com.kiloit.hms.db.reposiroty.RoleRepository;
import com.kiloit.hms.db.reposiroty.UserRepository;
import com.kiloit.hms.exception.httpstatus.BadRequestException;
import com.kiloit.hms.model.mapper.UpdateProfileMapper;
import com.kiloit.hms.model.mapper.UpdateUserMapper;
import com.kiloit.hms.model.projection.UserEntityInfo;
import com.kiloit.hms.model.request.auth.PasswordResetRQ;
import com.kiloit.hms.model.request.user.UpdateProfileRQDto;
import com.kiloit.hms.model.request.user.UpdateUserRQ;
import com.kiloit.hms.model.request.user.UpdateUserRQDto;
import com.kiloit.hms.model.request.user.UserRQ;
import com.kiloit.hms.security.UserPrincipal;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class Userservice extends BaseService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final S3Buckets s3Buckets;
    private final S3Service s3Service;
    private final ImageUploadService imageUpload;
    private final UpdateUserMapper updateUserMapper;
    private final UpdateProfileMapper updateProfileMapper;


    public StructureRS getUser(BaseListingRQ request, Long roleId) {
        if (roleId != 0 && !roleRepository.existsById(roleId)) {
            throw new BadRequestException((MessageConstant.ROLE.ROLE_ID_NOT_FOUND));
        }
        Page<UserEntityInfo> user = userRepository.findByNameStartsWith(request.getQuery(), request.getPageable(request.getSort(), request.getOrder()), roleId);
        return response(user.getContent(), user);
    }

    public StructureRS showById(Long id) {
        Optional<UserEntityInfo> optionalUserEntityUserEntity = Optional.ofNullable(userRepository.findByIdUser(id));
        if (optionalUserEntityUserEntity.isEmpty()) {
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);
        }
        return response(optionalUserEntityUserEntity.get());
    }

    public StructureRS addUser(@RequestBody UserRQ userRQ) {
        UserEntity userEntity = new UserEntity();
        if (userRepository.existsByEmailIgnoreSoftDelete(userRQ.getEmail()) > 0){
            throw new BadRequestException(MessageConstant.USERS.EMAIL_EXISTED);
        }
        if (userRepository.existsByUsernameIgnoreSoftDelete(userRQ.getUsername()) > 0){
            throw new BadRequestException(MessageConstant.USERS.USERNAME_EXISTED);
        }
        userEntity.setUsername(userRQ.getUsername());
        userEntity.setEmail(userRQ.getEmail());
        userEntity.setPassword(this.passwordEncoder.encode(userRQ.getPassword()));
        userEntity.setPhone(userRQ.getPhone());
        userEntity.setAddress(userRQ.getAddress());
        userEntity.setName(userRQ.getName());
        userEntity.setBio(userRQ.getBio());
        userEntity.setDate_of_birth(userRQ.getDateOfBirth());
        userEntity.setStatus(Boolean.TRUE);
        userEntity.setRoleEntity(roleRepository.getReferenceById(userRQ.getRoleId()));
        userRepository.save(userEntity);
        return response(MessageConstant.USERS.USERS_ADDED);
    }

    public StructureRS updateUser(UpdateUserRQDto updateUserRQDto, Long id) {
        if (id == 1) throw new BadRequestException(MessageConstant.NOT_ALLOWED);
        Optional<UserEntity> optionalUserEntityUserEntity = userRepository.findById(id);
        if (optionalUserEntityUserEntity.isEmpty()) {
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);
        }

        if(updateUserRQDto.getEmail() != null && userRepository.existsByEmailIgnoreUserIdAndSoftDelete(updateUserRQDto.getEmail(), id)>0){
            throw new BadRequestException(MessageConstant.USERS.EMAIL_EXISTED);
        }

        if(updateUserRQDto.getUsername() != null && userRepository.existsByUsernameIgnoreUserIdAndSoftDelete(updateUserRQDto.getUsername(), id)>0){
            throw new BadRequestException(MessageConstant.USERS.USERNAME_EXISTED);
        }


        UserEntity userEntity = updateUserMapper.partialUpdate(updateUserRQDto, optionalUserEntityUserEntity.get());

        userRepository.save(userEntity);
        return response(MessageConstant.USERS.UERS_UPDATED);
    }

    public StructureRS deleteUser(Long id) {
        if (id == 1) throw new BadRequestException(MessageConstant.NOT_ALLOWED);
        Optional<UserEntity> optionalUserEntityUserEntity = userRepository.findById(id);
        if (optionalUserEntityUserEntity.isEmpty()) {
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);
        }
        userRepository.deleteById(id);
        return response(MessageConstant.USERS.UERS_DELETED);
    }

    public StructureRS changePasswordUser(Long id, PasswordResetRQ request) {
        if (id == 1) throw new BadRequestException(MessageConstant.NOT_ALLOWED);
        Optional<UserEntity> optionalUserEntityuserEntity = userRepository.findById(id);
        if (optionalUserEntityuserEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);

        UserEntity user = optionalUserEntityuserEntity.get();
        user.setPassword(this.passwordEncoder.encode(request.getPassword()));
        userRepository.save(user);
        return response(MessageConstant.USERS.USER_IS_UPDATE);
    }

    public StructureRS resetPassword(PasswordResetRQ request, UserPrincipal principal) {
        if (principal.getId() == 1) throw new BadRequestException(MessageConstant.NOT_ALLOWED);
        Optional<UserEntity> userEntity = userRepository.findById(principal.getId());

        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);

        if (!passwordEncoder.matches(request.getOldPassword(), userEntity.get().getPassword()))
            throw new BadRequestException(MessageConstant.USERS.OLD_PASSWORD_WRONG);

        if (passwordEncoder.matches(request.getPassword(), userEntity.get().getPassword()))
            throw new BadRequestException(MessageConstant.USERS.SAME_PASSWORD);

        UserEntity user = userEntity.get();
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        userRepository.save(user);

        return response(MessageConstant.USERS.USER_IS_UPDATE);
    }

    public StructureRS addImage( MultipartFile image, Long id) throws IOException {
        Optional<UserEntity> userEntity = userRepository.findById(id);

        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);

        if (image.isEmpty())
            throw new BadRequestException(MessageConstant.USERS.PROFILE_IMAGE_NOT_FOUND);

        if (
                        !image.getContentType().equals("image/jpeg") &&
                        !image.getContentType().equals("image/jpg") &&
                        !image.getContentType().equals("image/png")
        )
            throw new BadRequestException(MessageConstant.USERS.PROFILE_IMAGE_NOT_VALID);

        if (image.getSize() > 1024 * 1024 * 10)
            throw new BadRequestException(MessageConstant.USERS.IMAGE_TOO_LARGE);
        String url;
        try {
            String uniqueFileName = "avatar/users" + UUID.randomUUID().toString() + s3Service.generateFileName(image);
            File file = s3Service.convertMultiPartToFile(image);
            url = s3Buckets.getEndPointUrl() + "/" + uniqueFileName;
            imageUpload.uploadImageToS3(uniqueFileName, file);
            file.delete();
        }catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        userEntity.get().setAvatar(url);
        userRepository.save(userEntity.get());
        return response(MessageConstant.USERS.IMAGE_ADDED);
    }
    public StructureRS getProfile(UserPrincipal userPrincipal) {
        Optional<UserEntityInfo> userEntity = userRepository.findByIdProfile(userPrincipal.getId());
        if (userEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);
        }
        return response(userEntity.get());
    }

    public StructureRS updateProfile(UpdateProfileRQDto updateProfileRQDto, UserPrincipal userPrincipal) {
        if(userPrincipal.getId() == 1) throw new BadRequestException(MessageConstant.NOT_ALLOWED);
        Optional<UserEntity> userEntity = userRepository.findById(userPrincipal.getId());
        if (userEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);
        }

        if(
                updateProfileRQDto.getUsername() != null
                && userRepository.existsByUsernameIgnoreUserIdAndSoftDelete(updateProfileRQDto.getUsername(), userPrincipal.getId())>0)
            throw new BadRequestException(MessageConstant.USERS.USERNAME_EXISTED);

        if(
                updateProfileRQDto.getEmail() != null
                        && userRepository.existsByEmailIgnoreUserIdAndSoftDelete(updateProfileRQDto.getEmail(), userPrincipal.getId())>0)
            throw new BadRequestException(MessageConstant.USERS.EMAIL_EXISTED);


        UserEntity user = updateProfileMapper.partialUpdate(updateProfileRQDto, userEntity.get());
        userRepository.save(user);
        return response(MessageConstant.USERS.UERS_UPDATED);
    }


    public StructureRS addImageByToken(UserPrincipal user, MultipartFile file) {
        if (user.getId()==1)
            throw new BadRequestException(MessageConstant.NOT_ALLOWED);
        Optional<UserEntity> userEntity = userRepository.findById(user.getId());
        if (userEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);
        }
        if (file.isEmpty())
            throw new BadRequestException(MessageConstant.USERS.PROFILE_IMAGE_NOT_FOUND);

        if (
                !file.getContentType().equals("image/jpeg") &&
                !file.getContentType().equals("image/jpg") &&
                !file.getContentType().equals("image/png")
        )
            throw new BadRequestException(MessageConstant.USERS.PROFILE_IMAGE_NOT_VALID);

        if (file.getSize() > 1024 * 1024 * 10)
            throw new BadRequestException(MessageConstant.USERS.IMAGE_TOO_LARGE);
        String url;
        try {
            String uniqueFileName = "avatar/users" + UUID.randomUUID().toString() + s3Service.generateFileName(file);
            File fileToken = s3Service.convertMultiPartToFile(file);
            url = s3Buckets.getEndPointUrl() + "/" + uniqueFileName;
            imageUpload.uploadImageToS3(uniqueFileName, fileToken);
            fileToken.delete();
        }catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        userEntity.get().setAvatar(url);
        userRepository.save(userEntity.get());
        return response(MessageConstant.USERS.IMAGE_ADDED);
    }

    public StructureRS deleteImageByToken(UserPrincipal build) {
        Optional<UserEntity> userEntity = userRepository.findById(build.getId());
        if (userEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.USERS.ID_NOT_FOUND);
        }
        userEntity.get().setAvatar(null);
        userRepository.save(userEntity.get());
        return response(MessageConstant.USERS.IMAGE_DELETED);
    }
}
