CREATE TABLE booking
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    customer_id    BIGINT                NULL,
    user_id        BIGINT                NULL,
    check_in       datetime              NULL,
    check_out      datetime              NULL,
    status         VARCHAR(255)          NULL,
    payment_status VARCHAR(255)          NULL,
    created_by     BIGINT                NULL,
    updated_by     BIGINT                NULL,
    created_at     datetime              NULL,
    updated_at     datetime              NULL,
    CONSTRAINT pk_booking PRIMARY KEY (id)
);

CREATE TABLE booking_detail
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    booking_id BIGINT                NULL,
    room_id    BIGINT                NULL,
    price      DOUBLE                NOT NULL,
    total_day  INT                   NULL,
    created_by BIGINT                NULL,
    updated_by BIGINT                NULL,
    created_at datetime              NULL,
    updated_at datetime              NULL,
    CONSTRAINT pk_booking_detail PRIMARY KEY (id)
);

CREATE TABLE customer
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    name       VARCHAR(255)          NOT NULL,
    email      VARCHAR(255)          NOT NULL,
    phone      VARCHAR(255)          NOT NULL,
    created_by BIGINT                NULL,
    updated_by BIGINT                NULL,
    created_at datetime              NULL,
    updated_at datetime              NULL,
    CONSTRAINT pk_customer PRIMARY KEY (id)
);

CREATE TABLE daily_report
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    total_booking INT                   NULL,
    total_cancel  INT                   NULL,
    total_income  DOUBLE                NOT NULL,
    total_expense DOUBLE                NOT NULL,
    created_by    BIGINT                NULL,
    updated_by    BIGINT                NULL,
    created_at    datetime              NULL,
    updated_at    datetime              NULL,
    CONSTRAINT pk_daily_report PRIMARY KEY (id)
);

CREATE TABLE hotel_info
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    title         VARCHAR(100)          NULL,
    sub_title     VARCHAR(100)          NULL,
    `description` VARCHAR(255)          NULL,
    thumbnail_url VARCHAR(255)          NULL,
    created_by    BIGINT                NULL,
    updated_by    BIGINT                NULL,
    created_at    datetime              NULL,
    updated_at    datetime              NULL,
    CONSTRAINT pk_hotel_info PRIMARY KEY (id)
);

CREATE TABLE payment
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    booking_id     BIGINT                NULL,
    total_payment  DOUBLE                NOT NULL,
    payment_status VARCHAR(255)          NULL,
    payment_date   datetime              NULL,
    created_by     BIGINT                NULL,
    updated_by     BIGINT                NULL,
    created_at     datetime              NULL,
    updated_at     datetime              NULL,
    CONSTRAINT pk_payment PRIMARY KEY (id)
);

CREATE TABLE permission
(
    id     BIGINT AUTO_INCREMENT NOT NULL,
    name   VARCHAR(255)          NOT NULL,
    module VARCHAR(255)          NOT NULL,
    CONSTRAINT pk_permission PRIMARY KEY (id)
);

CREATE TABLE review
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    user_id       BIGINT                NOT NULL,
    title         VARCHAR(255)          NULL,
    `description` VARCHAR(255)          NULL,
    created_by    BIGINT                NULL,
    updated_by    BIGINT                NULL,
    created_at    datetime              NULL,
    updated_at    datetime              NULL,
    CONSTRAINT pk_review PRIMARY KEY (id)
);

CREATE TABLE `role`
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    name       VARCHAR(255)          NULL,
    code       VARCHAR(255)          NULL,
    created_by BIGINT                NULL,
    updated_by BIGINT                NULL,
    created_at datetime              NULL,
    updated_at datetime              NULL,
    CONSTRAINT pk_role PRIMARY KEY (id)
);

CREATE TABLE roles_has_permissions
(
    permission_id BIGINT NOT NULL,
    role_id       BIGINT NOT NULL
);

CREATE TABLE room
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    room_type_id BIGINT                NOT NULL,
    floor        VARCHAR(255)          NULL,
    room         VARCHAR(255)          NULL,
    status       VARCHAR(255)          NULL,
    created_by   BIGINT                NULL,
    updated_by   BIGINT                NULL,
    created_at   datetime              NULL,
    updated_at   datetime              NULL,
    CONSTRAINT pk_room PRIMARY KEY (id)
);

CREATE TABLE room_type
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    title         VARCHAR(100)          NULL,
    sub_title     VARCHAR(100)          NULL,
    `description` VARCHAR(255)          NULL,
    bed           INT                   NULL,
    adult         INT                   NULL,
    children      INT                   NULL,
    price         DOUBLE                NULL,
    amenity       VARCHAR(255)          NULL,
    created_by    BIGINT                NULL,
    updated_by    BIGINT                NULL,
    created_at    datetime              NULL,
    updated_at    datetime              NULL,
    CONSTRAINT pk_room_type PRIMARY KEY (id)
);

CREATE TABLE room_type_image
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    room_type_id BIGINT                NOT NULL,
    url          VARCHAR(255)          NULL,
    created_by   BIGINT                NULL,
    updated_by   BIGINT                NULL,
    created_at   datetime              NULL,
    updated_at   datetime              NULL,
    CONSTRAINT pk_room_type_image PRIMARY KEY (id)
);

CREATE TABLE users
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    name          VARCHAR(100)          NULL,
    username      VARCHAR(255)          NOT NULL,
    email         VARCHAR(100)          NULL,
    password      VARCHAR(100)          NOT NULL,
    bio           VARCHAR(200)          NULL,
    avatar        VARCHAR(200)          NULL,
    address       VARCHAR(200)          NULL,
    phone         VARCHAR(50)           NULL,
    date_of_birth VARCHAR(50)           NULL,
    status        TINYINT(1) DEFAULT 1  NULL,
    role_id       BIGINT                NOT NULL,
    created_by    BIGINT                NULL,
    updated_by    BIGINT                NULL,
    created_at    datetime              NULL,
    updated_at    datetime              NULL,
    CONSTRAINT pk_users PRIMARY KEY (id)
);

ALTER TABLE payment
    ADD CONSTRAINT uc_payment_booking UNIQUE (booking_id);

CREATE INDEX idx_permission_name ON permission (name);

CREATE UNIQUE INDEX idx_role_name ON `role` (name);

CREATE UNIQUE INDEX idx_user_email ON users (email);

CREATE UNIQUE INDEX idx_user_username ON users (username);

ALTER TABLE booking_detail
    ADD CONSTRAINT FK_BOOKING_DETAIL_ON_BOOKING FOREIGN KEY (booking_id) REFERENCES booking (id);

ALTER TABLE booking_detail
    ADD CONSTRAINT FK_BOOKING_DETAIL_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE booking_detail
    ADD CONSTRAINT FK_BOOKING_DETAIL_ON_ROOM FOREIGN KEY (room_id) REFERENCES room (id);

ALTER TABLE booking_detail
    ADD CONSTRAINT FK_BOOKING_DETAIL_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE booking
    ADD CONSTRAINT FK_BOOKING_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE booking
    ADD CONSTRAINT FK_BOOKING_ON_CUSTOMER FOREIGN KEY (customer_id) REFERENCES customer (id);

ALTER TABLE booking
    ADD CONSTRAINT FK_BOOKING_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE booking
    ADD CONSTRAINT FK_BOOKING_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE customer
    ADD CONSTRAINT FK_CUSTOMER_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE customer
    ADD CONSTRAINT FK_CUSTOMER_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE daily_report
    ADD CONSTRAINT FK_DAILY_REPORT_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE daily_report
    ADD CONSTRAINT FK_DAILY_REPORT_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE hotel_info
    ADD CONSTRAINT FK_HOTEL_INFO_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE hotel_info
    ADD CONSTRAINT FK_HOTEL_INFO_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE payment
    ADD CONSTRAINT FK_PAYMENT_ON_BOOKING FOREIGN KEY (booking_id) REFERENCES booking (id);

ALTER TABLE payment
    ADD CONSTRAINT FK_PAYMENT_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE payment
    ADD CONSTRAINT FK_PAYMENT_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE review
    ADD CONSTRAINT FK_REVIEW_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE review
    ADD CONSTRAINT FK_REVIEW_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE review
    ADD CONSTRAINT FK_REVIEW_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE `role`
    ADD CONSTRAINT FK_ROLE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE `role`
    ADD CONSTRAINT FK_ROLE_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE room
    ADD CONSTRAINT FK_ROOM_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE room
    ADD CONSTRAINT FK_ROOM_ON_ROOM_TYPE FOREIGN KEY (room_type_id) REFERENCES room_type (id);

ALTER TABLE room
    ADD CONSTRAINT FK_ROOM_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE room_type_image
    ADD CONSTRAINT FK_ROOM_TYPE_IMAGE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE room_type_image
    ADD CONSTRAINT FK_ROOM_TYPE_IMAGE_ON_ROOM_TYPE FOREIGN KEY (room_type_id) REFERENCES room_type (id);

ALTER TABLE room_type_image
    ADD CONSTRAINT FK_ROOM_TYPE_IMAGE_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE room_type
    ADD CONSTRAINT FK_ROOM_TYPE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE room_type
    ADD CONSTRAINT FK_ROOM_TYPE_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE users
    ADD CONSTRAINT FK_USERS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE users
    ADD CONSTRAINT FK_USERS_ON_ROLE FOREIGN KEY (role_id) REFERENCES `role` (id);

ALTER TABLE users
    ADD CONSTRAINT FK_USERS_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE roles_has_permissions
    ADD CONSTRAINT fk_rolhasper_on_permission_entity FOREIGN KEY (permission_id) REFERENCES permission (id);

ALTER TABLE roles_has_permissions
    ADD CONSTRAINT fk_rolhasper_on_role_entity FOREIGN KEY (role_id) REFERENCES `role` (id);
