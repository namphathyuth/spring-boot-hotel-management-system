ALTER TABLE booking
DROP
FOREIGN KEY FK_BOOKING_ON_CUSTOMER;

ALTER TABLE booking_detail
    ADD adult INT NULL AFTER room_id;

ALTER TABLE booking_detail
    ADD child INT NULL AFTER adult;

ALTER TABLE booking_detail
    ADD room_type_id BIGINT NULL AFTER booking_id;

ALTER TABLE booking
    ADD special_request VARCHAR(255) NULL AFTER check_out;

ALTER TABLE booking_detail
    ADD CONSTRAINT FK_BOOKING_DETAIL_ON_ROOM_TYPE FOREIGN KEY (room_type_id) REFERENCES room_type (id);

ALTER TABLE booking
DROP
COLUMN customer_id;

ALTER TABLE booking
DROP
COLUMN payment_status;

ALTER TABLE booking_detail
DROP
COLUMN price;

ALTER TABLE booking_detail
DROP
COLUMN total_day;