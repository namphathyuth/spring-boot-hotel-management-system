ALTER TABLE booking
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE booking_detail
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE customer
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE daily_report
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE hotel_info
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE payment
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE review
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE `role`
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE room
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE room_type
    ADD deleted_at datetime NULL AFTER updated_at;

ALTER TABLE room_type_image
    ADD deleted_at datetime NULL AFTER updated_at;