ALTER TABLE payment
    ADD payment_method VARCHAR(255) NULL AFTER payment_status;

ALTER TABLE payment
    ADD remark VARCHAR(255) NULL AFTER payment_method;