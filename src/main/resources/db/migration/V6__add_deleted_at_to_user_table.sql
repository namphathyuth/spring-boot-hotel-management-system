ALTER TABLE users
    ADD deleted_at datetime NULL AFTER updated_at;